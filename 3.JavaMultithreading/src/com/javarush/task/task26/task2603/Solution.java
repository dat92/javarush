package com.javarush.task.task26.task2603;

import java.util.Comparator;
import java.util.TreeSet;

/*
Убежденному убеждать других не трудно
*/
public class Solution {

    public static void main(String[] args) {

    }

    public static class CustomizedComparator<T> implements Comparator<T> {

        private Comparator<T>[] comparators;

        public CustomizedComparator(Comparator<T>... vararg) {
            comparators = vararg;
        }

        public int compare(T o1, T o2) {
            int resComparat = 0;
            for (Comparator comparator : comparators) {
                resComparat = comparator.compare(o1, o2);
                if (resComparat != 0) break;
            }
            return resComparat;
        }

    }
}
