package com.javarush.task.task38.task3803;

/* 
Runtime исключения (unchecked exception)
*/

public class VeryComplexClass {
    public void methodThrowsClassCastException() {
        Object o = Integer.valueOf(1);
        String s = (String) o;
    }

    public void methodThrowsNullPointerException() {
        String nString = null;
        nString.charAt(0);
    }

    public static void main(String[] args) {
        VeryComplexClass test = new VeryComplexClass();
        test.methodThrowsClassCastException();
        test.methodThrowsNullPointerException();
    }
}
