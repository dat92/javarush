package com.javarush.task.task31.task3102;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/* 
Находим все файлы
*/
public class Solution {
    public static List<String> getFileTree(String root) throws IOException {
        List<String> result = new LinkedList<>();
        LinkedBlockingQueue<File> fileQueue = new LinkedBlockingQueue<>();
        fileQueue.offer(new File(root));

        while (fileQueue.size() > 0) {
            File curFile = fileQueue.poll();

            if (curFile.isDirectory()) {
                File[] files = curFile.listFiles();
                if (null != files && files.length > 0) {
                    for (File file : files) {
                        if (file.isDirectory()) fileQueue.offer(file);
                        else result.add(file.getAbsolutePath());
                    }
                }
            } else result.add(curFile.getAbsolutePath());
        }
        return result;
    }

    public static void main(String[] args) {
        List<String> files = null;
        try {
            files = getFileTree("/home/dat");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        for (String cur : files) {
            System.out.println(cur);
        }
    }
}
