package com.javarush.task.task21.task2101;

/* 
Определяем адрес сети
*/
public class Solution {
    public static void main(String[] args) {
        byte[] ip = new byte[]{(byte) 192, (byte) 168, 1, 2};
        byte[] mask = new byte[]{(byte) 255, (byte) 255, (byte) 254, 0};
        byte[] netAddress = getNetAddress(ip, mask);
        print(ip);          //11000000 10101000 00000001 00000010
        print(mask);        //11111111 11111111 11111110 00000000
        print(netAddress);  //11000000 10101000 00000000 00000000
    }

    public static byte[] getNetAddress(byte[] ip, byte[] mask) {
        //Проверим на возможные ошибки - массивы должны быть одинаковой длинны
        if (ip.length != mask.length) return null;
        byte[] address = new byte[ip.length];
        for (int i=0; i<ip.length; i++) {
            //Результат конъюнкции int, приводим назад к byte
            address[i] = (byte) (ip[i] & mask[i]);
        }
        return address;
    }

    public static void print(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        //Идём в обратном порядке, чтобы числа не записались наоборот
        for (int j=bytes.length-1; j>=0; j-- ) {
            int x = 1;
            //Byte имеет размер 8 бит
            for (int i = 0; i < 8; i++) {
                /*
                Получаем по битам число в двоичной системе
                & с единицей даст нам тот же бит, что и в текущем числе
                 */
                sb.append((bytes[j] & x) == 0 ? "0":"1");
                //Идём к следующему разряду
                x<<=1;
            }
            //Следующее число
            sb.append(" ");
        }
        sb.reverse();
        System.out.println(sb);
    }
}
