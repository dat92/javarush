package com.javarush.task.task30.task3009;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* 
Палиндром?
*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(getRadix("112"));        //expected output: [3, 27, 13, 15]
        System.out.println(getRadix("123"));        //expected output: [6]
        System.out.println(getRadix("5321"));       //expected output: []
        System.out.println(getRadix("1A"));         //expected output: []
    }

    private static Set<Integer> getRadix(String number) {
        int valDec;

        try {
            valDec = Integer.parseInt(number, 10);
        } catch (NumberFormatException ex) {
            return Collections.emptySet();
        }

        Set<Integer> result = new HashSet<>();

        for (int i = 2; i <= 36; i++) {
            String val = Integer.toString(valDec, i);

            boolean isPalindrom = true;
            for (int j = 0; j <= (val.length() / 2); j++) {
                if (val.charAt(j) != val.charAt((val.length() - 1) - j)) {
                    isPalindrom = false;
                    break;
                }
            }

            if (isPalindrom) result.add(i);
        }
        return result;
    }
}