package com.javarush.task.task31.task3107;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
Null Object Pattern
*/
public class Solution {
    private FileData fileData;

    public Solution(String pathToFile) {
        try {
            Path file = Paths.get(pathToFile);
            //if (Files.notExists(file)) throw new FileNotFoundException();
            fileData = new ConcreteFileData(
                    Files.isHidden(file),
                    Files.isExecutable(file),
                    Files.isDirectory(file),
                    Files.isWritable(file));
        }
        catch (IOException ex) {
            fileData = new NullFileData(ex);
        }
    }

    public FileData getFileData() {
        return fileData;
    }
}
