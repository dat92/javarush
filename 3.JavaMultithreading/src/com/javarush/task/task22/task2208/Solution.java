package com.javarush.task.task22.task2208;

import java.util.Map;

/* 
Формируем WHERE
*/
public class Solution {
    public static void main(String[] args) {

    }
    public static String getQuery(Map<String, String> params) {
        StringBuilder sb = new StringBuilder("");
        for (Map.Entry<String, String> entry : params.entrySet()) {
            String value = entry.getValue();
            if (null == value) continue;
            String key = entry.getKey();
            sb.append(key).append(" = '").append(value).append("'").append(" and ");
        }
        if (sb.length() > 0) {
            sb.delete(sb.length() - 5, sb.length());
        }
        return sb.toString();
    }
}
