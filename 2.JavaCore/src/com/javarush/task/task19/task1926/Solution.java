package com.javarush.task.task19.task1926;

/* 
Перевертыши
*/

import java.awt.*;
import java.io.*;

public class Solution {
    public static void main(String[] args) {
        String fileName;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            fileName = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while (reader.ready()) {
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine());
                System.out.println(sb.reverse());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
