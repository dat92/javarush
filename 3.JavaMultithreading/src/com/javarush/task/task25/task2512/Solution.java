package com.javarush.task.task25.task2512;

import java.util.*;

/*
Живем своим умом
*/
public class Solution implements Thread.UncaughtExceptionHandler {

    @Override
    public void uncaughtException(Thread t, Throwable e) {

        t.interrupt();
        List<Throwable> throwables = new ArrayList<>();
        throwables.add(e);
        Throwable lastThrowable = e;
        while (lastThrowable.getCause() != null) {
            throwables.add(lastThrowable.getCause());
            lastThrowable = lastThrowable.getCause();
        }

        Collections.reverse(throwables);

        throwables.forEach(System.out::println);

    }

    public static void main(String[] args) throws Exception {
        /*
        new Solution().uncaughtException(Thread.currentThread(),
                new Exception("ABC",
                        new RuntimeException("DEF", new IllegalAccessException("GHI"))));
        */
    }
}
