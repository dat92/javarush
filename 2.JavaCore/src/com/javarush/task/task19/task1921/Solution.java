package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Wrong args");
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            while (reader.ready()) {
                String cur = reader.readLine();
                String name = cur.replaceAll("\\d", "").trim();
                //System.out.println(name);
                String dateStr = cur.replaceAll(name, "").trim();
                //System.out.println(dateStr);

                DateFormat format = new SimpleDateFormat("dd MM yyyy");
                Date date;
                try {
                    date = format.parse(dateStr);
                } catch (ParseException ex) {
                    ex.printStackTrace();
                    continue;
                }
                PEOPLE.add(new Person(name, date));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
    }
}
