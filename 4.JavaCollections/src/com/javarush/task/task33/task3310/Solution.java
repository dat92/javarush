package com.javarush.task.task33.task3310;

import com.javarush.task.task33.task3310.strategy.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static Set<Long> getIds(Shortener shortener, Set<String> strings) {
        HashSet<Long> ids = new HashSet<>();
        for (String curStr : strings) {
            ids.add(shortener.getId(curStr));
        }
        return ids;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys) {
        HashSet<String> strings = new HashSet<>();
        for (Long curLong : keys) {
            strings.add(shortener.getString(curLong));
        }
        return strings;
    }

    public static void testStrategy(StorageStrategy strategy, long elementNumber) {
        Helper.printMessage(strategy.getClass().getSimpleName());
        HashSet<String> testStrings = new HashSet<>(
                elementNumber >= Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) elementNumber);
        for (long i = 0; i < elementNumber; i++) {
            testStrings.add(Helper.generateRandomString());
        }

        Shortener shortener = new Shortener(strategy);

        Date dateStart = new Date();
        Set<Long> ids = getIds(shortener, testStrings);
        Date dateEnd = new Date();
        Helper.printMessage(dateEnd.getTime() - dateStart.getTime() + "");

        dateStart = new Date();
        Set<String> resultString = getStrings(shortener, ids);
        dateEnd = new Date();
        Helper.printMessage(dateEnd.getTime() - dateStart.getTime() + "");

        Helper.printMessage(testStrings.size() == resultString.size() ?
                "Тест пройден." : "Тест не пройден.");
    }



    public static void main(String[] args) {
        testStrategy(new HashMapStorageStrategy(), 30_000);
        testStrategy(new OurHashMapStorageStrategy(), 10_000);
        testStrategy(new FileStorageStrategy(), 500);
        testStrategy(new OurHashBiMapStorageStrategy(), 10_000);
        testStrategy(new HashBiMapStorageStrategy(), 10_000);
        testStrategy(new DualHashBidiMapStorageStrategy(), 30_000);
    }
}
