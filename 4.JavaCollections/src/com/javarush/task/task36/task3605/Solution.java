package com.javarush.task.task36.task3605;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;

/* 
Использование TreeSet
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        if (args == null || args.length == 0) return;
        TreeSet<Character> set = new TreeSet<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            String cur;
            while ((cur = reader.readLine()) != null) {
                char[] characters = cur.toLowerCase().toCharArray();
                for (Character character : characters) {
                    set.add(character);
                }
            }
        }

        SortedSet<Character> res = set.subSet('a', '{');
        int len = 5;
        for (Character character : res) {
            if (len-- == 0) break;
            System.out.print(character);
        }
        System.out.println();

    }
}
