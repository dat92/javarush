package com.javarush.task.task30.task3008.client;

import com.javarush.task.task30.task3008.ConsoleHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BotClient extends Client {

    @Override
    protected String getUserName() {
        return "date_bot_" + (int) (Math.random() * 100);
    }

    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    public class BotSocketThread extends SocketThread {
        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день," +
                    " месяц, год, время, час, минуты, секунды.");
            super.clientMainLoop();
        }

        @Override
        protected void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
            int separatorIndex = message.indexOf(": ");
            if (separatorIndex == -1) return;
            String userName = message.substring(0, separatorIndex);
            String command = message.substring(separatorIndex+2);
            System.out.println(userName);
            System.out.println(command);
            String dateFormat;
            switch (command) {
                case "дата":
                    dateFormat = "d.MM.YYYY";
                    break;
                case "день":
                    dateFormat = "d";
                    break;
                case "месяц":
                    dateFormat = "MMMM";
                    break;
                case "год":
                    dateFormat = "YYYY";
                    break;
                case "время":
                    dateFormat = "H:mm:ss";
                    break;
                case "час":
                    dateFormat = "H";
                    break;
                case "минуты":
                    dateFormat = "m";
                    break;
                case "секунды":
                    dateFormat = "s";
                    break;
                default:
                    return;
            }

            SimpleDateFormat dFormat = new SimpleDateFormat(dateFormat);
            String result = "Информация для " + userName + ": "
                    + dFormat.format(Calendar.getInstance().getTime());
            sendTextMessage(result);
        }
    }

    public static void main(String[] args) {
        BotClient client = new BotClient();
        client.run();
    }
}
