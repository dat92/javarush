package com.javarush.task.task27.task2712.statistic.event;

import com.javarush.task.task27.task2712.RestaurantDate;
import com.javarush.task.task27.task2712.kitchen.Dish;

import java.util.Date;
import java.util.List;

public class CookedOrderEventDataRow implements EventDataRow {
    private Date currentDate;
    private String tabletName;
    private String cookName;
    private int cookingTimeSeconds;
    private List<Dish> cookingDishs;

    public CookedOrderEventDataRow(
            String tabletName, //Назва планшета
            String cookName, //Имя повара
            int cookingTimeSeconds, //Время приготовления в сек
            List<Dish> cookingDishs //Список блюд для приготовления
    ) {
        currentDate = new Date(RestaurantDate.getInstance().getTime());
        this.tabletName = tabletName;
        this.cookName = cookName;
        this.cookingTimeSeconds = cookingTimeSeconds;
        this.cookingDishs = cookingDishs;
    }

    public String getCookName() {
        return cookName;
    }

    @Override
    public EventType getType() {
        return EventType.COOKED_ORDER;
    }

    @Override
    public Date getDate() {
        return currentDate;
    }

    @Override
    public int getTime() {
        return cookingTimeSeconds;
    }
}
