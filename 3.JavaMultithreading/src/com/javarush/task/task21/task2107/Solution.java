package com.javarush.task.task21.task2107;

import java.util.LinkedHashMap;
import java.util.Map;

/* 
Глубокое клонирование карты
*/
public class Solution implements Cloneable {

    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.users.put("Hubert", new User(172, "Hubert"));
        solution.users.put("Zapp", new User(41, "Zapp"));
        Solution clone = null;
        try {
            clone = solution.clone();
            System.out.println(solution);
            System.out.println(clone);

            System.out.println(solution.users);
            System.out.println(clone.users);

        } catch (CloneNotSupportedException e) {
            e.printStackTrace(System.err);
        }
    }

    protected Map<String, User> users = new LinkedHashMap();

    protected Solution clone() throws CloneNotSupportedException {
        Solution cloneSol = new Solution();
        for (Map.Entry<String, User> entry : users.entrySet()) {
            cloneSol.users.put(entry.getKey(), entry.getValue().clone());
        }
        return cloneSol;
    }

    public static class User implements Cloneable {
        int age;
        String name;

        protected User clone() throws CloneNotSupportedException {
            return (User) super.clone();
        }

        public User(int age, String name) {
            this.age = age;
            this.name = name;
        }

        public boolean equals(Object n) {
            if (null == n) return false;
            if (n == this) return true;
            if (!(n instanceof User)) return false;
            User usr = (User) n;
            return (((null == name) && (null == usr.name)) || (name.equals(usr.name)));
        }

        public int hashCode() {
            int result=age;
            result += name == null ? 0 : name.hashCode();
            return 37 * result;
        }
    }
}
