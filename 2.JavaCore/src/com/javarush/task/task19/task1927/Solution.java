package com.javarush.task.task19.task1927;

/* 
Контекстная реклама
*/

import java.io.*;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        String adventure = "JavaRush - курсы Java онлайн";
        PrintStream dOut = System.out;
        ByteArrayOutputStream bArr = new ByteArrayOutputStream();
        PrintStream mOut = new PrintStream(bArr);

        System.setOut(mOut);
        testString.printSomething();
        System.setOut(dOut);

        String[] strArr = bArr.toString().split(System.lineSeparator());
        boolean isPrint = false;
        for (String curString : strArr) {
            System.out.println(curString);
            if (isPrint) {
                System.out.println(adventure);
                isPrint = false;
            } else {
                isPrint = true;
            }
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("first");
            System.out.println("second");
            System.out.println("third");
            System.out.println("fourth");
            System.out.println("fifth");
        }
    }
}
