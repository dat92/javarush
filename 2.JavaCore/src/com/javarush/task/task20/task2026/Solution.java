package com.javarush.task.task20.task2026;

import java.util.ArrayList;

/*
Алгоритмы-прямоугольники
*/
public class Solution {
    public static void main(String[] args) {
        byte[][] a1 = new byte[][]{
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {1, 1, 0, 1}
        };
        byte[][] a2 = new byte[][]{
                {1, 0, 0, 1},
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {1, 0, 0, 1}
        };

        byte[][] a3 = new byte[][] {
                {1, 1, 0, 1, 1, 0},
                {0, 0, 0, 1, 1, 0},
                {1, 1, 0, 1, 1, 0},
                {1, 1, 0, 1, 1, 0},
                {0, 0, 0, 0, 0, 0},
                {1, 0, 1, 0, 0, 1}
        };

        int count1 = getRectangleCount(a1);
        System.out.println("count = " + count1 + ". Должно быть 2");
        int count2 = getRectangleCount(a2);
        System.out.println("count = " + count2 + ". Должно быть 4");
        int count3 = getRectangleCount(a3);
        System.out.println("count = " + count3 + ". Должно быть 6");
    }

    public static int getRectangleCount(byte[][] a) {
        ArrayList<Rectangle> list = new ArrayList<>();
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<a[i].length; j++) {
                if (a[i][j] == 0) continue;
                if (list.size() == 0) {
                    list.add(new Rectangle());
                }
                boolean added = false;
                for (Rectangle cur : list) {
                    if (cur.addCoordinate(i, j)) added = true;
                }
                if (!added) {
                    Rectangle newR = new Rectangle();
                    newR.newCoordinate(i, j);
                    list.add(newR);
                }
            }
        }
        return list.size();
    }

    static class Rectangle {
        private ArrayList<int[]> coordinats;

        public Rectangle() {
            coordinats = new ArrayList<>();
        }

        private void newCoordinate(int a, int b) {
            int[] arr = new int[2];
            arr[0] = a;
            arr[1] = b;
            coordinats.add(arr);
        }

        private boolean checkCoordinate(int a, int b) {
            if (a < 0 || b < 0) return false;
            if (coordinats.size() == 0) {
                newCoordinate(a, b);
                return true;
            }

            for (int[] cur : coordinats) {
                if (a==cur[0] && b == cur[1]) return true; //Уже есть такая координата

                /*
                Подразумевается, что матрицу обходят построчно слева направо
                Поэтому диагонального варианта быть не может и новые числа
                могут быть только больше
                 */
                if ((cur[0] == a && (cur[1]+1) == b) ||
                        ((cur[0]+1) == a && cur[1] == b)) {
                    newCoordinate(a, b);
                    return true;
                }
            }
            return false;
        }

        public boolean addCoordinate(int a, int b) {
            return checkCoordinate(a, b);
        }
    }
}
