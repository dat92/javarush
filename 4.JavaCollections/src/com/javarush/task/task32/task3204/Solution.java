package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/* 
Генератор паролей
*/
public class Solution {
    private static Random random = new Random(System.currentTimeMillis());
    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        int passLen = 8;
        ByteArrayOutputStream password = new ByteArrayOutputStream();

        /*
        For guarantee a meeting misc requirements of password,
         we selecting positions in password with mandatory type of requires
         */
        int[] mandPositions = generateNonRepeatValues(3, passLen-1, 0);

        for (int i = 0; i < passLen; i++) {
            if (i == mandPositions[0]) {
                password.write(generateLowCaseChar());
                continue;
            } else if (i == mandPositions[1]) {
                password.write(generateUpperCaseChar());
                continue;
            } else if (i == mandPositions[2]) {
                password.write(generateNumber());
                continue;
            }

            int type = random.nextInt(2 + 1);
            switch (type) {
                case 0:
                    password.write(generateLowCaseChar());
                    break;
                case 1:
                    password.write(generateUpperCaseChar());
                    break;
                case 2:
                    password.write(generateNumber());
            }
        }

        return password;
    }

    private static int generateNumber() {
        return random.nextInt(57 - 48 + 1) + 48;
    }

    private static int generateUpperCaseChar() {
        return random.nextInt(90 - 65 + 1) + 65;
    }

    private static int generateLowCaseChar() {
        return random.nextInt(122 - 97 + 1) + 97;
    }

    private static int[] generateNonRepeatValues(int len, int max, int min) {
        Set<Integer> exceptVal = new HashSet<>();
        int[] result = new int[len];
        for (int i = 0; i < len; i++) {
            int cur;
            do {
                cur = random.nextInt(max - min + 1) + min;
            } while (exceptVal.contains(cur));

            exceptVal.add(cur);
            result[i] = cur;
        }
        return result;
    }
}