package com.javarush.task.task19.task1917;

/* 
Свой FileWriter
*/

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileWriter;
import java.io.IOException;

public class FileConsoleWriter {
    //Не наследовались от класса FileWriter
    private FileWriter fileWriter;

    public FileConsoleWriter(String s) throws IOException {
        fileWriter = new FileWriter(s);
    }

    public FileConsoleWriter(String s, boolean b) throws IOException {
        fileWriter = new FileWriter(s, b);
    }

    public FileConsoleWriter(File file) throws IOException {
        fileWriter = new FileWriter(file);
    }

    public FileConsoleWriter(File file, boolean b) throws IOException {
        fileWriter = new FileWriter(file, b);
    }

    public FileConsoleWriter(FileDescriptor fileDescriptor) {
        fileWriter = new FileWriter(fileDescriptor);
    }



    public void write(char[] cbuf, int off, int len) throws IOException {
        fileWriter.write(cbuf, off, len);
        System.out.println(String.valueOf(cbuf, off, len));
    }

    public void write(int c) throws IOException {
        fileWriter.write(c);
        System.out.println(c);
    }

    public void write(String str) throws IOException {
        fileWriter.write(str);
        System.out.println(str);
    }

    public void write(String str, int off, int len) throws IOException {
        fileWriter.write(str, off, len);
        System.out.println(str.substring(off, off+len));
    }

    public void write(char[] cbuf) throws IOException {
        fileWriter.write(cbuf);
        System.out.println(String.valueOf(cbuf));
    }

    public void close() throws IOException {
        fileWriter.close();
    }

    public static void main(String[] args) {
        if (args.length !=1) {
            System.err.println("Wrong argument");
            return;
        }
        FileConsoleWriter writer = null;
        //Не реализовли интерфейс AutoCloseable т.к. нет в условии
        try {
            writer = new FileConsoleWriter(args[0]);
            String str = "123456789";
            writer.write(str, 2,3);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (null != writer) {
                try {
                    writer.close();
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }
}