package com.javarush.task.task35.task3513;

public class MoveEfficiency implements Comparable<MoveEfficiency> {
    private int numberOfEmptyTiles;
    private int score;
    private Move move;

    public Move getMove() {
        return move;
    }

    MoveEfficiency(int numberOfEmptyTiles, int score, Move move) {
        this.numberOfEmptyTiles = numberOfEmptyTiles;
        this.score = score;
        this.move = move;
    }

    @Override
    public int compareTo(MoveEfficiency moveEfficiency) {
        int numEmpty = Integer.compare(numberOfEmptyTiles,
                moveEfficiency.numberOfEmptyTiles);
        if (numEmpty == 0) {
            return Integer.compare(score, moveEfficiency.score);
        }
        else {
            return numEmpty;
        }
    }
}
