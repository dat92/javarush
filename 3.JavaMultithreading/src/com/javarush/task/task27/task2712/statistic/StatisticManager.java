package com.javarush.task.task27.task2712.statistic;

import com.javarush.task.task27.task2712.statistic.event.CookedOrderEventDataRow;
import com.javarush.task.task27.task2712.statistic.event.EventDataRow;
import com.javarush.task.task27.task2712.statistic.event.EventType;
import com.javarush.task.task27.task2712.statistic.event.VideoSelectedEventDataRow;

import java.text.SimpleDateFormat;
import java.util.*;

public class StatisticManager {
    private static StatisticManager ourInstance = new StatisticManager();
    private StatisticStorage statisticStorage = new StatisticStorage();
    private SimpleDateFormat dateFormat =
            new SimpleDateFormat("dd-MMM-yyyy", Locale.US);

    public static StatisticManager getInstance() {
        return ourInstance;
    }

    private StatisticManager() {
    }

    public void register(EventDataRow data) {
        statisticStorage.put(data);
    }

    private class StatisticStorage {
        private Map<EventType, List<EventDataRow>> storage = new HashMap<>();

        public StatisticStorage() {
            for (EventType type : EventType.values()) {
                storage.put(type, new ArrayList<EventDataRow>());
            }
        }

        private void put(EventDataRow data) {
            List<EventDataRow> oldData = storage.get(data.getType());
            oldData.add(data);
        }

        private List<EventDataRow> get(EventType type) {
            return storage.get(type);
        }
    }

    public StatisticStorage getStatisticStorage() {
        return statisticStorage;
    }

    public Map<String, Long> getProfitFromAdvertisement() {
        Map<String, Long> profit = new TreeMap<>(Collections.reverseOrder());
        for (EventDataRow event : statisticStorage.get(EventType.SELECTED_VIDEOS)) {
            String date = dateFormat.format(event.getDate());
            if (! profit.containsKey(date)) {
                profit.put(date,
                        ((VideoSelectedEventDataRow) event).getAmount());
            } else {
                Long val = profit.get(date);
                val += ((VideoSelectedEventDataRow) event).getAmount();
                profit.put(date, val);
            }
        }
        return profit;
    }

    public Map<String, Map<String, Integer>> getCooksWorkTime() {
        Map<String, Map<String, Integer>> workTime =
                new TreeMap<>(Collections.reverseOrder());
        for (EventDataRow event : statisticStorage.get(EventType.COOKED_ORDER)) {
            CookedOrderEventDataRow cookEvent = (CookedOrderEventDataRow) event;
            String date = dateFormat.format(event.getDate());

            if (! workTime.containsKey(date)) {
                Map<String, Integer> cookWork = new TreeMap<>();
                cookWork.put(cookEvent.getCookName(), cookEvent.getTime());
                workTime.put(date, cookWork);
            } else {
                Map<String, Integer> cookForDate = workTime.get(date);
                if (! cookForDate.containsKey(cookEvent.getCookName())) {
                    cookForDate.put(cookEvent.getCookName(), cookEvent.getTime());
                } else {
                    Integer val = cookForDate.get(cookEvent.getCookName());
                    val += cookEvent.getTime();
                    cookForDate.put(cookEvent.getCookName(), val);
                }
            }
        }
        return workTime;
    }
}
