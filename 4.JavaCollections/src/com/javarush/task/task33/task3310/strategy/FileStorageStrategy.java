package com.javarush.task.task33.task3310.strategy;

import java.nio.file.Path;

public class FileStorageStrategy implements StorageStrategy {
    static final int DEFAULT_INITIAL_CAPACITY = 16;
    static final long DEFAULT_BUCKET_SIZE_LIMIT = 10_000;

    private FileBucket[] table = new FileBucket[DEFAULT_INITIAL_CAPACITY];
    int size;

    long maxBucketSize;
    private long bucketSizeLimit = DEFAULT_BUCKET_SIZE_LIMIT;


    public long getBucketSizeLimit() {
        return bucketSizeLimit;
    }

    public void setBucketSizeLimit(long bucketSizeLimit) {
        this.bucketSizeLimit = bucketSizeLimit;
    }

    public int hash(Long k) {
        int h;
        return (k == null) ? 0 : (h = k.hashCode()) ^ (h >>> 16);
    }

    public int indexFor(int hash, int length) {
        return hash & (length - 1);
    }

    public void resize(int newCapacity) {
        FileBucket[] newTable = new FileBucket[newCapacity];
        transfer(newTable);
        table = newTable;
    }

    void transfer(FileBucket[] newTable) {
        long newMaxBucket = 0; //renew maxBucket
        FileBucket[] src = table;
        int newCapacity = newTable.length;
        for (int i = 0; i < src.length; i++) {
            FileBucket oldBucket = src[i];
            if (oldBucket != null) {
                Entry entry = oldBucket.getEntry();
                if (entry != null) {
                    src[i] = null;
                    FileBucket newBucket = new FileBucket();
                    oldBucket.remove(); //Delete tmp files
                    do {
                        int index = indexFor(entry.hash, newCapacity);
                        Entry next = entry.next;
                        entry.next = newTable[index].getEntry();
                        newBucket.putEntry(entry);
                        newTable[index] = newBucket;
                        if (newBucket.getFileSize() > newMaxBucket) {
                            newMaxBucket = newBucket.getFileSize();
                        }
                        entry = next;
                    } while (entry != null);
                }
            }
        }
        maxBucketSize = newMaxBucket;
    }

    public void addEntry(int hash, Long key, String value, int bucketIndex) {
        FileBucket bucket = table[bucketIndex];
        if (bucket == null) {
            bucket = new FileBucket();
            bucket.putEntry(new Entry(hash, key, value, null));
        } else {
            Entry entry = bucket.getEntry();
            bucket.putEntry(new Entry(hash, key, value, entry));
        }
        table[bucketIndex] = bucket;

        if (bucket.getFileSize() > maxBucketSize) maxBucketSize = bucket.getFileSize();
        if (maxBucketSize > bucketSizeLimit) {
            resize(2 * table.length);
        }
    }

    public void createEntry(int hash, Long key, String value, int bucketIndex) {
        FileBucket bucket = table[bucketIndex];
        Entry entry = bucket.getEntry();
        bucket.putEntry(new Entry(hash, key, value, entry));
        table[bucketIndex] = bucket;

        if (bucket.getFileSize() > maxBucketSize) maxBucketSize = bucket.getFileSize();
        size++;
    }

    public Entry getEntry(Long key) {
        int hash = (key == null) ? 0 : hash(key);

        for (Entry e = table[indexFor(hash, table.length)].getEntry();
             e != null; e = e.next) {

            Long curKey;
            if (e.hash == hash && ((curKey = e.key) == key || (key != null && key.equals(curKey)))) {
                return e;
            }
        }
        return null;
    }

    @Override
    public boolean containsKey(Long key) {
        return getEntry(key) != null;
    }

    @Override
    public boolean containsValue(String value) {
        if (value == null) return false;
        FileBucket[] tab = table;
        for (FileBucket bucket : tab) {
            if (bucket == null) continue;
            for (Entry entry = bucket.getEntry(); entry != null; entry = entry.next) {
                if (value.equals(entry.value)) return true;
            }
        }
        return false;
    }

    @Override
    public void put(Long key, String value) {
        if (key == null) return;
        int hash = hash(key);
        int i = indexFor(hash, table.length);

        if (table[i] != null) {
            for (Entry entry = table[i].getEntry(); entry != null; entry = entry.next) {
                Long k;
                if (entry.hash == hash && ((k = entry.key) == key || key.equals(k))) {
                    entry.value = value;
                }
            }
        }
        addEntry(hash, key, value, i);
    }

    @Override
    public Long getKey(String value) {
        if (value == null) return null;
        FileBucket[] tab = table;
        for (FileBucket curBucket : tab) {
            if (curBucket == null) continue;
            for (Entry curEntryChain = curBucket.getEntry();
                 curEntryChain != null;
                 curEntryChain = curEntryChain.next) {

                if (value.equals(curEntryChain.value)) return curEntryChain.getKey();
            }
        }
        return null;
    }

    @Override
    public String getValue(Long key) {
        if (key == null) return null;
        int hash = hash(key);
        for (Entry entry = table[indexFor(hash, table.length)].getEntry();
             entry != null; entry = entry.next) {

            Long k;
            if (entry.hash == hash &&((k = entry.key) == key || key.equals(k))) {
                return entry.value;
            }
        }
        return null;
    }
}
