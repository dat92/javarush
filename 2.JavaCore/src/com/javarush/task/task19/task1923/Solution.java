package com.javarush.task.task19.task1923;

/* 
Слова с цифрами
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Wrong args");
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]));
             BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]))) {
            while (reader.ready()) {
                String[] curArray = reader.readLine().split(" ");
                for (String curWord : curArray) {
                    if (curWord.matches(".*\\d+.*")) {
                        writer.write(" " + curWord);
                    }
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
    }
}
