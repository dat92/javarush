package com.javarush.task.task27.task2712;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class RandomOrderGeneratorTask implements Runnable {
    private List<Tablet> tablets;
    private int interval;

    public RandomOrderGeneratorTask(List<Tablet> tablets, int interval) {
        this.tablets = tablets;
        this.interval = interval;
    }

    @Override
    public void run() {
        while (! Thread.currentThread().isInterrupted()) {
            int selectTablet = ThreadLocalRandom.current().nextInt(tablets.size());
            tablets.get(selectTablet).createTestOrder();
            try {
                TimeUnit.MILLISECONDS.sleep(interval);
            }
            catch (InterruptedException interrupt) {
                return;
            }
        }
    }
}

