package com.javarush.task.task27.task2712.kitchen;

import com.javarush.task.task27.task2712.ConsoleHelper;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Waiter implements Runnable {
    private LinkedBlockingQueue<Order> queue = new LinkedBlockingQueue<>();

    public void setQueue(LinkedBlockingQueue<Order> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        Thread waiterDaemon = new Thread(() -> {
            while (! Thread.currentThread().isInterrupted()) {
                if (queue.size() > 0) {
                    Order curOrder = queue.poll();
                    ConsoleHelper.writeMessage(curOrder + " was cooked by " + curOrder.getCook());
                } else {
                    try {
                        TimeUnit.MILLISECONDS.sleep(10);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        waiterDaemon.setDaemon(true);
        waiterDaemon.start();
    }
}
