package com.javarush.task.task38.task3804;

public class ExceptionFabric {
    public static <T extends Enum> Throwable getException(T enumVal) {
        Throwable throwable;

        if (enumVal == null) return new IllegalArgumentException();

        if (enumVal instanceof ApplicationExceptionMessage) {
            ApplicationExceptionMessage app = (ApplicationExceptionMessage) enumVal;
            throwable = new Exception(app.name());
        }
        else if (enumVal instanceof DatabaseExceptionMessage) {
            DatabaseExceptionMessage db = (DatabaseExceptionMessage) enumVal;
            throwable = new RuntimeException(db.name());
        }
        else if (enumVal instanceof UserExceptionMessage) {
            UserExceptionMessage user = (UserExceptionMessage) enumVal;
            throwable = new Error(user.name());
        }
        else return new IllegalArgumentException();

        String fixMessage = throwable.getMessage().substring(0,1).toUpperCase() +
                throwable.getMessage().substring(1).toLowerCase().
                replaceAll("_", " ");

        try {
            throw throwable;
        } catch (RuntimeException run) {
            return new RuntimeException(fixMessage);
        } catch (Exception ex) {
            return new Exception(fixMessage);
        } catch (Error err) {
            return new Error(fixMessage);
        } catch (Throwable ignore) {}

        return new IllegalArgumentException();
    }
}
