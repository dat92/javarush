package com.javarush.task.task27.task2712;

import java.util.Date;

public class RestaurantDate {
    public static final long DAY = 86400000;

    private boolean debug = false;
    private long time;

    private RestaurantDate() {
        time = new Date().getTime();
    }

    private static class DateHolder {
        private final static RestaurantDate instance = new RestaurantDate();
    }

    public static RestaurantDate getInstance() {
        return DateHolder.instance;
    }

    public long getTime() {
        return debug ? time : new Date().getTime();
    }

    public void changeTime(long value) {
        time = value;
        debug = true;
    }

    public void resetTime() {
        debug = false;
        time = new Date().getTime();
    }

    public void changeDay(int val) {
        changeTime(getTime() + (RestaurantDate.DAY * val));
    }
}
