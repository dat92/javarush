package com.javarush.task.task33.task3310.strategy;

public class OurHashMapStorageStrategy implements StorageStrategy {
    static final int DEFAULT_INITIAL_CAPACITY = 16;
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    Entry[] table = new Entry[DEFAULT_INITIAL_CAPACITY];
    int size;
    int threshold = (int) (DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
    float loadFactor = DEFAULT_LOAD_FACTOR;


    public int hash(Long k) {
        int h;
        return (k == null) ? 0 : (h = k.hashCode()) ^ (h >>> 16);
    }

    public int indexFor(int hash, int length) {
        return hash & (length - 1);
    }

    public Entry getEntry(Long key) {
        int hash = (key == null) ? 0 : hash(key);
        for (Entry e = table[indexFor(hash, table.length)]; e != null; e = e.next) {
            Long curKey;
            if (e.hash == hash &&
                    ((curKey = e.key) == key || (key != null && key.equals(curKey)))) {
                return e;
            }
        }
        return null;
    }

    public void resize(int newCapacity) {
        Entry[] newTable = new Entry[newCapacity];
        transfer(newTable);
        table = newTable;
        threshold = (int) (newCapacity * loadFactor);
    }

    public void transfer(Entry[] newTable) {
        Entry[] src = table;
        int newCapacity = newTable.length;
        for (int i = 0; i < src.length; i++) {
            Entry entry = src[i];
            if (entry != null) {
                src[i] = null;
                do {
                    int index = indexFor(entry.hash, newCapacity);
                    Entry next = entry.next;
                    entry.next = newTable[index];
                    newTable[index] = entry;
                    entry = next;
                } while (entry != null);
            }
        }
    }

    public void addEntry(int hash, Long key, String value, int bucketIndex) {
        Entry entry = table[bucketIndex];
        table[bucketIndex] = new Entry(hash, key, value, entry);
        if (size++ >= threshold) {
            resize(2 * table.length);
        }
    }

    public void createEntry(int hash, Long key, String value, int bucketIndex) {
        Entry entry = table[bucketIndex];
        table[bucketIndex] = new Entry(hash, key, value, entry);
        size++;
    }

    @Override
    public boolean containsKey(Long key) {
        return getEntry(key) != null;
    }

    @Override
    public boolean containsValue(String value) {
        if (value == null) return false;
        Entry[] tab = table;
        for (Entry item : tab) {
            for (Entry entry = item; entry != null; entry = entry.next) {
                if (value.equals(entry.value)) return true;
            }
        }
        return false;
    }

    @Override
    public void put(Long key, String value) {
        if (key == null) return;
        int hash = hash(key);
        int i = indexFor(hash, table.length);

        for (Entry entry = table[i]; entry != null; entry = entry.next) {
            Long k;
            if (entry.hash == hash && ((k = entry.key) == key || key.equals(k))) {
                entry.value = value;
            }
        }
        addEntry(hash, key, value, i);
    }

    @Override
    public Long getKey(String value) {
        if (value == null) return null;
        Entry[] tab = table;
        for (Entry curEntry : tab) {
            for (Entry curEntryChain = curEntry;
                 curEntryChain != null;
                 curEntryChain = curEntryChain.next) {

                if (value.equals(curEntryChain.value)) return curEntryChain.getKey();
            }
        }
        return null;
    }

    @Override
    public String getValue(Long key) {
        if (key == null) return null;
        int hash = hash(key);

        for (Entry entry = table[indexFor(hash, table.length)]; entry != null; entry = entry.next) {
            Long k;
            if (entry.hash == hash &&((k = entry.key) == key || key.equals(k))) {
                return entry.value;
            }
        }
        return null;
    }
}
