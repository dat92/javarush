package com.javarush.task.task32.task3210;

import java.io.IOException;
import java.io.RandomAccessFile;

/* 
Используем RandomAccessFile
*/

public class Solution {
    public static void main(String... args) {
        try (RandomAccessFile raf = new RandomAccessFile(args[0], "rw")) {
            int seekVal = Integer.parseInt(args[1]);
            raf.seek(seekVal);

            int lengthString = args[2].getBytes().length;
            byte[] bytes = new byte[lengthString];
            raf.read(bytes, 0, lengthString);
            String readStr = new String(bytes);
            raf.seek(raf.length());
            byte[] result = readStr.equals(args[2]) ? "true".getBytes() : "false".getBytes();
            raf.write(result);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
