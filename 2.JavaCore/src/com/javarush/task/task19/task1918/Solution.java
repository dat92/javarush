package com.javarush.task.task19.task1918;

/* 
Знакомство с тегами
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    private Map<Integer, Integer> indexes;

    public Solution() {
        indexes = new TreeMap<>();
    }

    public static void main(String[] args) {
        /*
        Использование JSoup и подобных библиотек быстрее и эффективней,
        но смысл задачи в том, чтобы написать parser самому.
         */
        if (args.length != 1) {
            System.err.println("Wrong argument");
            return;
        }

        Solution showTag = new Solution();
        String fileName = showTag.getFileName();
        if (fileName == null) return;

        String[] strings = showTag.getSubstringOfTag(fileName, args[0]);
        if (strings == null) return;

        for (String curStr : strings) {
            System.out.println(curStr);
        }
    }

    public String getFileName() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            return reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String getStringFromFile (String fileName) {
        StringBuilder stringFromFile = new StringBuilder();
        String separator = System.getProperty("line.separator");
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            /*
            Эффективнее было бы использовать например FileUtils,
            но условие требует FileReader
             */
            while ((line = reader.readLine()) != null) {
                stringFromFile.append(line).append(separator);
            }
            return stringFromFile.toString();

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String[] getSubstringOfTag(String fileName, String tag) {
        String stringFromFile = getStringFromFile(fileName);
        if (stringFromFile == null) return null;
        findOpenTags(tag, stringFromFile);
        findCloseTags(tag, stringFromFile);
        return selectSubstrings(stringFromFile);

    }

    private String[] selectSubstrings(String stringFromFile) {
        ArrayList<String> substrings = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : indexes.entrySet()) {
            substrings.add(stringFromFile.substring(entry.getKey(), entry.getValue())
                    .replaceAll("\n|\r\n", ""));
            /*
            В NIX-системах перенос строки \n, в windows - \r\n
             */
        }
        return substrings.toArray(new String[0]);
    }

    private void findCloseTags(String tag, String string) {
        StringBuilder sb = new StringBuilder(string);
        //Закрывающие теги ищем в обратном порядке - от вложенных к внешним
        ArrayList<Integer> keys = new ArrayList<>(indexes.keySet());
        for (int i=keys.size()-1; i>=0; i--) {
            for (int j=keys.get(i); j<string.length();) {
                int openIndex = sb.indexOf("</", j);
                if (openIndex == -1) return;
                int closeIndex = sb.indexOf(">", openIndex);
                if (closeIndex == -1) {
                    //По условию задачи, для каждого тега есть закрывающий
                    System.out.println("Incorrect syntax" + string);
                    return;
                }

                String curStr = sb.substring(openIndex + 2, closeIndex);
                if (curStr.equalsIgnoreCase(tag)) {
                    indexes.put(keys.get(i), closeIndex + 1);
                /*
                Нужно убрать текущий закрывающий тег из дальнейшего поиска,
                но при этом требуется сохранить оригинальную длинну строки,
                чтобы в map записать правильную пару.
                Заменяем тег </tag> на <!tag>.
                */
                    sb.replace(openIndex + 1, openIndex + 2, "!");
                    break;
                }
                j=+closeIndex+1;
            }
        }
    }

    private void findOpenTags(String tag, String string) {
        for (int i=0; i<string.length(); ) {
            int openIndex = string.indexOf("<", i);
            if (openIndex == -1) return;
            int closeIndex = string.indexOf(">", openIndex);
            if (closeIndex == -1) {
                //По условию задачи, для каждого тега есть закрывающий
                System.out.println("Incorrect syntax: " + string);
                return;
            }
            String curTag = string.substring(openIndex + 1, closeIndex).replaceAll("\n\b", "");

            if (curTag.startsWith(tag)) {
                indexes.put(openIndex, -1);
            }
            i=+closeIndex+1;
        }
    }
}
