package com.javarush.task.task27.task2712.kitchen;


import com.javarush.task.task27.task2712.ConsoleHelper;
import com.javarush.task.task27.task2712.statistic.StatisticManager;
import com.javarush.task.task27.task2712.statistic.event.CookedOrderEventDataRow;
import java.util.Objects;
import java.util.Observable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Cook extends Observable implements Runnable {
    private String name;
    private LinkedBlockingQueue<Order> queue = new LinkedBlockingQueue<>();
    private LinkedBlockingQueue<Order> readyOrders = new LinkedBlockingQueue<>();

    public void setQueue(LinkedBlockingQueue<Order> queue) {
        this.queue = queue;
    }

    public void setReadyOrders(LinkedBlockingQueue<Order> readyOrders) {
        this.readyOrders = readyOrders;
    }

    public Cook(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public void startCookingOrder(Order order) {
        Objects.requireNonNull(order);

        ConsoleHelper.writeMessage("Start cooking - " + order
                + ", cooking time " + (order.getTotalCookingTime() + "min"));
        StatisticManager.getInstance().register(
                new CookedOrderEventDataRow(
                        order.getTablet().toString(),
                        name,
                        order.getTotalCookingTime() * 60,
                        order.getDishes()
                )
        );

        order.setCook(this);
        try {
            Thread.sleep(order.getTotalCookingTime() * 10);
            readyOrders.put(order);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {
        Thread daemonThread = new Thread(() -> {
            while (! Thread.currentThread().isInterrupted()) {
                if (queue.size() > 0) {
                    startCookingOrder(queue.poll());
                } else {
                    try {
                        TimeUnit.MILLISECONDS.sleep(10);
                    }
                    catch (InterruptedException interrupt) {
                        return;
                    }
                }
            }
        });

        daemonThread.setDaemon(true);
        daemonThread.start();
    }
}
