package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.ad.Advertisement;
import com.javarush.task.task27.task2712.ad.StatisticAdvertisementManager;
import com.javarush.task.task27.task2712.statistic.StatisticManager;

import java.util.*;

public class DirectorTablet {
    public void printAdvertisementProfit() {
        Map<String, Long> profit =
                StatisticManager.getInstance().getProfitFromAdvertisement();
        double sum = 0d;
        for (Map.Entry<String, Long> entry : profit.entrySet()) {
            double curSum = (double) entry.getValue() / 100;
            String message = String.format(Locale.US, "%s - %.2f",
                    entry.getKey(), curSum);
            sum += curSum;
            ConsoleHelper.writeMessage(message);
        }
        ConsoleHelper.writeMessage(String.format(Locale.US, "Total - %.2f", sum));
    }

    public void printCookWorkloading() {
        Map<String, Map<String, Integer>> cooks =
                StatisticManager.getInstance().getCooksWorkTime();
        for (Map.Entry<String, Map<String, Integer>> entry : cooks.entrySet()) {
            ConsoleHelper.writeMessage(entry.getKey());
            for (Map.Entry<String, Integer> entryDay : entry.getValue().entrySet()) {
                String message = String.format("%s - %d min",
                        entryDay.getKey(),
                        (int) Math.ceil((double) entryDay.getValue()) / 60);
                ConsoleHelper.writeMessage(message);
            }
            ConsoleHelper.writeMessage("");
        }
    }

    public void printActiveVideoSet() {
        List<Advertisement> advertisements =
                StatisticAdvertisementManager.getInstance().
                        getAdvertisementsByStatus(true);
        Comparator<Advertisement> compareByName =
                (advertisement, t1) -> advertisement.getName().compareToIgnoreCase(t1.getName());

        Collections.sort(advertisements, compareByName);
        for (Advertisement curAdvertisement : advertisements) {
            ConsoleHelper.writeMessage(String.format("%s - %d",
                    curAdvertisement.getName(), curAdvertisement.getHits()));
        }
    }

    public void printArchivedVideoSet() {
        List<Advertisement> advertisements =
                StatisticAdvertisementManager.getInstance().
                        getAdvertisementsByStatus(false);
        Comparator<Advertisement> compareByName =
                (advertisement, t1) -> advertisement.getName().compareToIgnoreCase(t1.getName());

        Collections.sort(advertisements, compareByName);
        advertisements.forEach(a -> ConsoleHelper.writeMessage(a.getName()));
    }
}
