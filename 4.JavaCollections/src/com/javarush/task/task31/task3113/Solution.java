package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;


class FolderVisitor extends SimpleFileVisitor<Path> {
    private long totalSize = 0;
    private int folderCount = 0;
    private int fileCount = 0;

    public long getTotalSize() {
        return totalSize;
    }

    public int getFolderCount() {
        //Так как корневая папка тоже вызывается
        return folderCount == 0 ? 0 : folderCount-1;
    }

    public int getFileCount() {
        return fileCount;
    }


    @Override
    public FileVisitResult preVisitDirectory(Path path, BasicFileAttributes basicFileAttributes) throws IOException {
        folderCount++;
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path path, BasicFileAttributes basicFileAttributes) throws IOException {
        fileCount++;
        totalSize += basicFileAttributes.size();
        return FileVisitResult.CONTINUE;
    }


    //Переопределил после валидации. Будет ругаться - убрать.
    @Override
    public FileVisitResult visitFileFailed(Path path, IOException e) throws IOException {
        if (e instanceof AccessDeniedException) {
            System.out.printf("%s - Access denied, skipp%n", path);
            if (Files.isDirectory(path)) return FileVisitResult.SKIP_SUBTREE;
            else return FileVisitResult.CONTINUE;
        } else {
            e.printStackTrace();
            return FileVisitResult.SKIP_SUBTREE;
        }
    }
}

/* 
Что внутри папки?
*/
public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Path folder = Paths.get(reader.readLine());

        if (Files.notExists(folder) || ! Files.isDirectory(folder)) {
            System.out.printf("%s - не папка", folder);
            return;
        }

        FolderVisitor fVisit = new FolderVisitor();

        Files.walkFileTree(folder, fVisit);
        System.out.printf("%s - %d%n", "Всего папок", fVisit.getFolderCount());
        System.out.printf("%s - %d%n", "Всего файлов", fVisit.getFileCount());
        System.out.printf("%s - %d%n", "Общий размер", fVisit.getTotalSize());
    }
}
