package com.javarush.task.task38.task3804;

/* 
Фабрика исключений
*/

public class Solution {
    public static Class getFactoryClass() {
        return ExceptionFabric.class;
    }

    public static void main(String[] args) throws Throwable {
        //throw ExceptionFabric.getException(ApplicationExceptionMessage.SOCKET_IS_CLOSED);
        //throw ExceptionFabric.getException(ApplicationExceptionMessage.UNHANDLED_EXCEPTION);

        //throw ExceptionFabric.getException(DatabaseExceptionMessage.NO_RESULT_DUE_TO_TIMEOUT);
        //throw ExceptionFabric.getException(DatabaseExceptionMessage.NOT_ENOUGH_CONNECTIONS);

        //throw ExceptionFabric.getException(UserExceptionMessage.USER_DOES_NOT_EXIST);
        //throw ExceptionFabric.getException(UserExceptionMessage.USER_DOES_NOT_HAVE_PERMISSIONS);
    }
}