package com.javarush.task.task20.task2022;

import java.io.*;

/* 
Переопределение сериализации в потоке
*/
public class Solution implements Serializable, AutoCloseable {
    transient private FileOutputStream stream;
    String fileName;

    public Solution(String fileName) throws FileNotFoundException {
        this.stream = new FileOutputStream(fileName);
        this.fileName = fileName;
    }

    public void writeObject(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.stream = new FileOutputStream(fileName, true);
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }

    public static void main(String[] args) throws Exception {
        String fileData = "test" + File.separatorChar + "task2022";
        Solution solution = new Solution(fileData);
        solution.writeObject("test");
        String fileSerializ = "test" + File.separatorChar + "task2022.dat";
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(new FileOutputStream(fileSerializ))) {
            oos.writeObject(solution);
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

        Solution load;
        try (ObjectInputStream ois =
                     new ObjectInputStream(new FileInputStream(fileSerializ))) {
            load = (Solution) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

        load.writeObject("123");
        solution.close();
        load.close();
    }
}
