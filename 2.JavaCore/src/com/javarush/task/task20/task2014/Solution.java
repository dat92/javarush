package com.javarush.task.task20.task2014;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* 
Serializable Solution
*/
public class Solution implements Serializable {
    public static void main(String[] args)
    {
        //System.out.println(new Solution(4));

        File file = new File("test" + File.separatorChar + "task2014");
        Solution savedObject = new Solution(10);

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(savedObject);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Solution loadedObject = new Solution(-1);
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            loadedObject = (Solution) ois.readObject();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println(savedObject);
        System.out.println(savedObject.temperature);
        System.out.println(loadedObject);
        System.out.println(loadedObject.temperature);
    }

    transient private final String pattern = "dd MMMM yyyy, EEEE";
    transient private Date currentDate;
    transient private int temperature;
    String string;

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today is %s, and the current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    @Override
    public String toString() {
        return this.string;
    }
}
