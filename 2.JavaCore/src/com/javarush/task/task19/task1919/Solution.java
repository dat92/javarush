package com.javarush.task.task19.task1919;

/* 
Считаем зарплаты
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Wrong Arguments");
            return;
        }

        Map<String, Double> values = new TreeMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            while (reader.ready()) {
                String[] cur = reader.readLine().split(" ");
                if (cur.length != 2) {
                    System.out.println("Incorrect entry");
                    continue;
                }
                double dVal;
                try {
                    dVal = values.get(cur[0]);
                } catch (NullPointerException ex) {
                    dVal = 0;
                }
                double newDVal;
                try {
                    newDVal = Double.parseDouble(cur[1]);
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                    continue;
                }
                dVal+=newDVal;
                values.put(cur[0], dVal);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        for (Map.Entry<String, Double> entry : values.entrySet()) {
            System.out.print(entry.getKey());
            System.out.println(" "+ entry.getValue());
        }
    }
}
