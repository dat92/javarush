package com.javarush.task.task19.task1920;

/* 
Самый богатый
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Wrong arguments");
            return;
        }

        Map<String, Double> values = new TreeMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            while (reader.ready()) {
                String[] curVal = reader.readLine().split(" ");
                if (curVal.length != 2) {
                    System.out.println("Incorrect enter date");
                    continue;
                }

                double curDouble;
                try {
                    curDouble = values.get(curVal[0]);
                } catch (NullPointerException ex) {
                    curDouble = 0;
                }

                double newDouble;
                try {
                    newDouble = Double.parseDouble(curVal[1]);
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                    continue;
                }

                values.put(curVal[0], newDouble + curDouble);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        //System.out.println(values);

        double maxVal=0;
        ArrayList<String> maxRich = new ArrayList<>();

        for (Map.Entry<String, Double> entry : values.entrySet()) {
            if (maxVal <= entry.getValue()) {
                maxVal = entry.getValue();
                maxRich.add(entry.getKey());
            }
        }

        Collections.sort(maxRich);
        for (String cur : maxRich) {
            System.out.println(cur);
        }
    }
}
