package com.javarush.task.task37.task3701;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Consumer;

/*
Круговой итератор
*/
public class Solution<T> extends ArrayList<T> {
    public static void main(String[] args) {
        Solution<Integer> list = new Solution<>();
        list.add(1);
        list.add(2);
        list.add(3);

        int count = 0;
        for (Integer i : list) {
            //1 2 3 1 2 3 1 2 3 1
            System.out.print(i + " ");
            count++;
            if (count == 10) {
                break;
            }
        }

        Iterator<Integer> it = list.iterator();
        System.out.println();
        int repeat = 0;
        while (it.hasNext()) {
            if (repeat++ == 10) {
                it.remove();
                System.out.println();
            }
            System.out.print(it.next() + " ");
            if (repeat == 20) {
                System.out.println();
                break;
            }
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new RoundIterator();
    }

    public class RoundIterator implements Iterator<T> {
        private Iterator<T> iterator;

        public RoundIterator() {
            resetIterator();
        }

        @Override
        public void remove() {
            iterator.remove();
        }

        @Override
        public boolean hasNext() {
            return size() != 0;
        }

        @Override
        public T next() {
            if (! iterator.hasNext()) {
                resetIterator();
            }
            return iterator.next();
        }

        @Override
        public void forEachRemaining(Consumer<? super T> action) {
            iterator.forEachRemaining(action);
        }

        private void resetIterator() {
            iterator = Solution.super.iterator();
        }
    }
}
