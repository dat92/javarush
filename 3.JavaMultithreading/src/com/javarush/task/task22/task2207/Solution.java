package com.javarush.task.task22.task2207;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/* 
Обращенные слова
*/
public class Solution {
    public static List<Pair> result = new LinkedList<>();

    public static void main(String[] args) {
        String fileName = null;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            fileName = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (null == fileName) return;

        List<String> words = new ArrayList<>();
        try {
            Files.lines(Paths.get(fileName), StandardCharsets.UTF_8).
                    forEach(s -> words.addAll(Arrays.asList(s.split(" "))));
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        for (int i=0; i<words.size()-1; i++) {
            if (null == words.get(i)) continue;
            StringBuilder sb = new StringBuilder(words.get(i));
            sb.reverse();
            for (int j=i+1; j<words.size(); j++) {
                if (sb.toString().equals(words.get(j))) {
                    Pair pair = new Pair();
                    pair.first = words.get(i);
                    pair.second = words.get(j);
                    Solution.result.add(pair);
                    /*
                    Затираем найденные слова в списке,
                    чтобы они не мешали находить пары.
                    В противном случае будут искаться "триады",
                     "тетрады" и т.д.
                    */
                    words.set(i, null);
                    words.set(j, null);
                    break;
                }
            }
        }

        result.forEach(System.out::println);

    }

    public static class Pair {
        String first;
        String second;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pair pair = (Pair) o;

            if (first != null ? !first.equals(pair.first) : pair.first != null) return false;
            return second != null ? second.equals(pair.second) : pair.second == null;

        }

        @Override
        public int hashCode() {
            int result = first != null ? first.hashCode() : 0;
            result = 31 * result + (second != null ? second.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return  first == null && second == null ? "" :
                    first == null && second != null ? second :
                    second == null && first != null ? first :
                    first.compareTo(second) < 0 ? first + " " + second : second + " " + first;

        }
    }

}
