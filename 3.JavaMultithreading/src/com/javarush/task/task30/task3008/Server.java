package com.javarush.task.task30.task3008;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    private static Map<String, Connection> connectionMap = new ConcurrentHashMap<>();

    public static void sendBroadcastMessage(Message message) {
        for (Map.Entry<String, Connection> entry : connectionMap.entrySet()) {
            try {
                entry.getValue().send(message);
            }
            catch (IOException e) {
                e.printStackTrace();
                ConsoleHelper.writeMessage("Сообщение пользователю \""
                        + entry.getKey()
                + "\" не было доставлено");
            }
        }
    }

    private static class Handler extends Thread {
        private Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        private String serverHandshake(Connection connection)
                throws IOException, ClassNotFoundException {

            while (true) {
                connection.send(new Message(MessageType.NAME_REQUEST));
                Message message = connection.receive();
                if (message.getType() == MessageType.USER_NAME &&
                        !message.getData().isEmpty() &&
                        !connectionMap.containsKey(message.getData())) {
                    connectionMap.put(message.getData(), connection);
                    connection.send(new Message(MessageType.NAME_ACCEPTED));
                    return message.getData();
                }
            }
        }

        private void sendListOfUsers(Connection connection, String userName)
                throws IOException {

            for (Map.Entry<String, Connection> entry : connectionMap.entrySet()) {
                String curName = entry.getKey();
                if (curName.equalsIgnoreCase(userName)) continue;
                Message message = new Message(MessageType.USER_ADDED, curName);
                connection.send(message);

            }
        }

        private void serverMainLoop(Connection connection, String userName)
                throws IOException, ClassNotFoundException {

            while (true) {
                Message message = connection.receive();
                if (message.getType() == MessageType.TEXT) {
                    Message modMessage = new Message(MessageType.TEXT,
                            userName + ": " + message.getData());
                    sendBroadcastMessage(modMessage);
                } else {
                    ConsoleHelper.writeMessage("Ошибка: сообщение не является" +
                            " типом TEXT");
                }
            }
        }

        public void run() {
            String remoteSocketAddress = socket.getRemoteSocketAddress().toString();
            String userName = null;
            ConsoleHelper.writeMessage("Установлено новое соединение с удалённым" +
                    " адресом:\"" + remoteSocketAddress + "\"");
            try (Connection connection = new Connection(socket)) {
                userName = serverHandshake(connection);
                sendBroadcastMessage(new Message(MessageType.USER_ADDED, userName));
                sendListOfUsers(connection, userName);
                serverMainLoop(connection, userName);
            } catch (IOException | ClassNotFoundException e) {
                ConsoleHelper.writeMessage("Ошибка: Произошла ошибка при обмене " +
                        "данными с удалённым адресом.");
            } finally {
                if (userName != null) {
                    connectionMap.remove(userName);
                    sendBroadcastMessage(new Message(MessageType.USER_REMOVED, userName));
                }
                ConsoleHelper.writeMessage("Соединение с удалённым адресом:\""
                        + remoteSocketAddress + "\" было закрыто.");
            }
        }
    }

    public static void main(String[] args) {
        ConsoleHelper.writeMessage("Введите номер порта для сервера");
        int port = ConsoleHelper.readInt();
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            ConsoleHelper.writeMessage("Сервер запущен");
            while (true) {
                new Handler(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ConsoleHelper.writeMessage("Сервер остановлен");
        }
    }
}
