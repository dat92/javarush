package com.javarush.task.task33.task3310.strategy;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileBucket {
    private Path path;

    public FileBucket() {
        try {
            path = Files.createTempFile(null, null);
            Files.deleteIfExists(path);
            path.toFile().deleteOnExit();
            Files.createFile(path);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long getFileSize() {
        try {
            return Files.size(path);
        }
        catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void putEntry(Entry entry) {
        try (ObjectOutputStream oos = new ObjectOutputStream(
                Files.newOutputStream(path)
        )) {
            oos.writeObject(entry);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public Entry getEntry() {
        try (ObjectInputStream ois = new ObjectInputStream(
                Files.newInputStream(path))) {
            if (getFileSize() > 0) {
                return (Entry) ois.readObject();
            }
        }
        catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void remove() {
        try {
            Files.delete(path);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
