package com.javarush.task.task30.task3010;

/* 
Минимальное допустимое основание системы счисления
*/


import java.math.BigInteger;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        try {
            String val = args[0].toUpperCase();
            if (!val.matches("[A-Z0-9]*")) {
                System.out.println("incorrect");
                return;
            }


            for (int i = 2; i <= Character.MAX_RADIX; i++) {
                try {
                    //Long принимать не хочет - не влезает
                    BigInteger test = new BigInteger(val, i);
                    System.out.println(i);
                    return;
                }
                catch (NumberFormatException ex) {
                }
            }

            System.out.println("incorrect");
        } catch (Exception ex) {
        }

    }
}