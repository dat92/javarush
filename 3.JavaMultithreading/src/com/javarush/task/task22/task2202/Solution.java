package com.javarush.task.task22.task2202;

/* 
Найти подстроку
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getPartOfString("JavaRush - лучший сервис обучения Java."));
        System.out.println(getPartOfString("Амиго и Диего лучшие друзья!"));
    }

    public static String getPartOfString(String string) throws TooShortStringException {
        if (null == string) throw new TooShortStringException();
        int firstSpaceIndex = string.indexOf(" ");
        if (firstSpaceIndex == -1) throw new TooShortStringException();
        int fourthSpaceIndex = firstSpaceIndex;
        for (int i=0; i<3; i++) { //По очереди ищём четвёртый пробел
            fourthSpaceIndex = string.indexOf(" ", fourthSpaceIndex+1);
            if (fourthSpaceIndex == -1) throw new TooShortStringException();
        }
        int lastIndex = string.indexOf(" ", fourthSpaceIndex+1);
        if (lastIndex == -1) { //Пробелов, а равно как и слов, больше нет
            lastIndex = string.length();
        }
        return string.substring(firstSpaceIndex+1, lastIndex);
    }

    public static class TooShortStringException extends RuntimeException {
    }
}
