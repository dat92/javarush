package com.javarush.task.task20.task2027;

import java.util.ArrayList;
import java.util.List;

/* 
Кроссворд
*/
public class Solution {
    public static void main(String[] args) {
        int[][] crossword = new int[][]{
                {'f', 'd', 'e', 'r', 'l', 'k'},
                {'u', 's', 'a', 'm', 'e', 'o'},
                {'l', 'n', 'g', 'r', 'o', 'v'},
                {'m', 'l', 'p', 'r', 'r', 'h'},
                {'p', 'o', 'e', 'e', 'j', 'j'}
        };
        List<Word> words = detectAllWords(crossword, "home", "same", "ore", "ovh");
        /*
Ожидаемый результат
home - (5, 3) - (2, 0)
same - (1, 1) - (4, 1)
         */
        for (Word curWord : words) {
            System.out.println(curWord);
        }
    }

    public static List<Word> detectAllWords(int[][] crossword, String... words) {
        List<Word> listWords = new ArrayList<>();
        // По условию в массиве есть все слова
        for (String curWordStr : words) {
            listWords.add(findNewWord(crossword, curWordStr));
        }
        return listWords;
    }

    /*
    Перечисление векторов с привязкой к сторонам света
    (Север наверху)
     */
    enum Vectors {
        Northwest(-1, -1), West(0, -1), Southwest(1, -1),
        North(-1, 0), Center(0, 0), South(1, 0),
        Northeast(-1, 1), East(0, 1), Southeast(1, 1);

        private int x; //Смещение по оси x
        private int y; //Смещение по оси y

        Vectors(int x, int y) {
            this.x = x;
            this.y = y;
        }


        int getX() {return x;}
        int getY() {return y;}
    }

    /**
     * Ищет новое слово в массиве
     * @param array - исходный массив
     * @param wordName - слово для поиска
     * @return - класс со словом
     */
    private static Word findNewWord(int[][] array, String wordName) {
        Word word = new Word(wordName);
        //Ищем букву с которой начинается слво
        for (int x=0; x<array.length; x++) {
            for (int y=0; y<array[x].length; y++) {
                if (array[x][y] == wordName.charAt(0)) {
                    /*
                    Передаём слово без первой буквы, т.к. полагаем что её нашли
                    и пытаемся найти следующую
                     */
                    int[] endChar = findSecondChar(array, x, y, wordName.substring(1));
                    if (endChar[0] != -1) { //Нашли все символы слова
                        /*
                        Требуется выводить сначала индекс "строки", потом "столбца"
                        А мы обрабатывали координаты с точки зрения массива
                        т.е. x - массив нулевого уровня, y - первого
                        Потому даём параметры в таком порядке
                         */
                        word.setStartPoint(y,x);
                        word.setEndPoint(endChar[1], endChar[0]);
                        return word;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Получаем требуемый вектор из двух параметров
     * @param x - смещение по x
     * @param y - смещение по y
     * @return - получившийся вектор
     */
    private static Vectors getVector(int x, int y) {
        switch (x) {
            case -1:
                switch (y) {
                    case -1:
                        return Vectors.Northwest;
                    case 0:
                        return Vectors.North;
                    case 1:
                        return Vectors.Northeast;
                }
                break;
            case 0:
                switch (y) {
                    case -1:
                        return Vectors.West;
                    case 0:
                        return Vectors.Center;
                    case 1:
                        return Vectors.East;
                }
                break;
            case 1:
                switch (y) {
                    case -1:
                        return Vectors.Southwest;
                    case 0:
                        return Vectors.South;
                    case 1:
                        return Vectors.Southeast;
                }
        }
        return null;
    }

    /**
     * Ищет окончание слова по заданному вектору
     * @param array - исходный массив
     * @param x - индекс массива нулевого уровня со второй буквой слова
     * @param y - индекс массива первого уровня со второй буковй слова
     * @param vector - вычисленный вектор для поиска
     * @param wordName - часть слова (без первых двух букв) для поиска
     * @return - массив с индексами (сначала нулевого, потом первого уровня)
     * последней буквы слова. Если не найдено в массиве будет -1 и -1
     */
    private static int[] findAllChars(int[][] array, int x, int y,
                                      Vectors vector, String wordName) {
        /*
        Отдельная процедура с ветором нужа по причине, что слова
        могут находится в массиве под прямыми линиями, без комбинаций.
        Поиск по методу findSecondChar может вернуть правильное слово,
        но с буквой (буквами) координата которой находятся не по прямой
        линии. Может "зацепить" рядом стоящую такую же букву.
         */
        //Вычисляем координаты предполагаемой следующей буквы
        int newX = x+vector.getX();
        int newY = y+vector.getY();
        if (array[newX][newY] == wordName.charAt(0)) {
            //Последняя буква - дальше искать нечего. Выходим из рекурсии.
            if (wordName.length() == 1) return new int[]{newX, newY};
            else {
                //Ищем следующие буквы по такому же вектору
                return findAllChars(array, newX, newY, vector, wordName.substring(1));
            }
        } else {
            //Следующая буква не подходит - пошли по ложному пути
            return new int[]{-1, -1};
        }
    }

    /**
     * Ищет следующий символ и задаёт дальнейший вектор поиска
     * @param array - исходный массив
     * @param x - индекс массива нулевого уровня
     * @param y - индекс массива первого уровня
     * @param wordName - оставшаяся часть слова для поиска
     * @return - массив с индексами последнего символа слова в случае если такой будет найден,
     * если нет - массив с элементами -1 и -1
     */
    private static int[] findSecondChar(int[][] array,
                                        int x, int y, String wordName) {
        /*
        Обходим "вокруг" заданной точки по массиву
        начиная с левой верхней точки (северо-запад)
         */
        for (int xi=x-1; xi<=x+1; xi++) {
            for (int yi = y-1; yi<=y+1; yi++) {
                if (xi == x && yi == y) continue; //Та же самая ячейка
                /*
                Проверяем, что это не граница массива
                Чтобы не поймать иссключение
                 */
                if ((xi < 0 || xi >= array.length)
                || (yi < 0 || yi >= array[x].length)) continue;

                if (array[xi][yi] == wordName.charAt(0)) {
                    //Считаем, что слово должно состоять хотя бы из двух букв
                    if (wordName.length() == 1) return new int[]{yi, xi};
                    /*
                    Определяем вектор, т.е. в какой стороне от буквы в массиве
                    нашли следующую
                     */
                    Vectors vector = getVector(xi-x, yi-y);
                    //Пытаемся найти остальные буквы по заданному вектору
                    int[] result = findAllChars(array, xi, yi, vector, wordName.substring(1));
                    if (result[0] != -1) return result;
                    /*
                    Если по такому вектору слова не нашли,
                    то пробуем поискать в соседней "ячейке" массива:
                    рядом могут быть одинаковые буквы
                     */
                }
            }
        }
        return new int[]{-1, -1};
    }

    public static class Word {
        private String text;
        private int startX;
        private int startY;
        private int endX;
        private int endY;

        public Word(String text) {
            this.text = text;
        }

        public void setStartPoint(int i, int j) {
            startX = i;
            startY = j;
        }

        public void setEndPoint(int i, int j) {
            endX = i;
            endY = j;
        }

        @Override
        public String toString() {
            return String.format("%s - (%d, %d) - (%d, %d)", text, startX, startY, endX, endY);
        }
    }
}
