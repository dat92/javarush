package com.javarush.task.task27.task2707;

import java.io.IOException;

/*
Определяем порядок захвата монитора
*/
public class Solution {
    public void someMethodWithSynchronizedBlocks(Object obj1, Object obj2) {
        synchronized (obj1) {
            synchronized (obj2) {
                System.out.println(obj1 + " " + obj2);
            }
        }
    }

    public static boolean isLockOrderNormal(final Solution solution, final Object o1, final Object o2) throws Exception {
        //do something here
        Thread testRun1 = new Thread() {
            @Override
            public void run() {
                synchronized (o1) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    synchronized (o2) {}
                }
            }
        };

        Thread testRun2 = new Thread() {
            @Override
            public void run() {
                solution.someMethodWithSynchronizedBlocks(o1, o2);
            }
        };

        testRun1.setDaemon(true);
        testRun2.setDaemon(true);
        testRun1.start();
        testRun2.start();
        do {
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        } while (testRun1.getState() == Thread.State.RUNNABLE
                || testRun2.getState() == Thread.State.RUNNABLE);

        System.out.println(testRun1.getState());
        System.out.println(testRun2.getState());
        return ((testRun1.getState() != Thread.State.BLOCKED)
                && (testRun2.getState() != Thread.State.BLOCKED));
    }

    public static void main(String[] args) throws Exception {
        final Solution solution = new Solution();
        final Object o1 = new Object();
        final Object o2 = new Object();

        System.out.println(isLockOrderNormal(solution, o1, o2));
    }
}
