package com.javarush.task.task37.task3714;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.LinkedList;

/* 
Древний Рим
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Input a roman number to be converted to decimal: ");
        String romanString = bufferedReader.readLine();
        System.out.println("Conversion result equals " + romanToInteger(romanString));
    }

    public static int romanToInteger(String s) {
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            int curVal = RomNumbers.valueOf(s.charAt(i) + "").getDecVal();
            if (i+1 < s.length()) {
                int nextVal = RomNumbers.valueOf(s.charAt(i+1) + "").decVal;
                if (curVal >= nextVal) sum += curVal;
                else {
                    sum += (nextVal - curVal);
                    i++;
                }
            } else {
                sum += curVal;
            }
        }
        return sum;
    }

    enum RomNumbers {
        I(1),
        V(5),
        X(10),
        L(50),
        C(100),
        D(500),
        M(1000);

        private int decVal;

        RomNumbers (int decVal) {
            this.decVal = decVal;
        }

        int getDecVal() {
            return decVal;
        }
    }
}
