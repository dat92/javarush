package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.ad.AdvertisementManager;
import com.javarush.task.task27.task2712.ad.NoVideoAvailableException;
import com.javarush.task.task27.task2712.kitchen.Order;
import com.javarush.task.task27.task2712.kitchen.TestOrder;
import com.javarush.task.task27.task2712.statistic.StatisticManager;
import com.javarush.task.task27.task2712.statistic.event.NoAvailableVideoEventDataRow;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Tablet {
    final int number;

    static Logger logger = Logger.getLogger(Tablet.class.getName());
    private LinkedBlockingQueue<Order> queue = new LinkedBlockingQueue<>();

    public Tablet(int number) {
        this.number = number;
    }

    public void setQueue(LinkedBlockingQueue<Order> queue) {
        this.queue = queue;
    }

    public Order createOrder() {
        Order order;
        try {
            order = new Order(this);
        }
        catch (IOException e) {
            logger.log(Level.SEVERE, "Console is unavailable.");
            return null;
        }
        return getOrder(order);
    }

    public void createTestOrder() {
        TestOrder order;
        try {
            order = new TestOrder(this);
        }
        catch (IOException e) {
            logger.log(Level.SEVERE, "Console is unavailable.");
            return;
        }
        getOrder(order);
    }

    private Order getOrder(Order order) {
        if (order.isEmpty()) return null;

        AdvertisementManager adManager =
                new AdvertisementManager(order.getTotalCookingTime() * 60);
        try {
            adManager.processVideos();
        }
        catch (NoVideoAvailableException ex) {
            logger.log(Level.INFO, "No video is available for the order " + order);
            StatisticManager.getInstance().register(
                    new NoAvailableVideoEventDataRow(order.getTotalCookingTime() * 60)
            );
        }

        try {
            queue.put(order);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public String toString() {
        return "Tablet{" +
                "number=" + number +
                '}';
    }


}
