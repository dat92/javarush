package com.javarush.task.task30.task3003;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TransferQueue;

public class Consumer implements Runnable {
    private TransferQueue<ShareItem> queue;

    public Consumer(TransferQueue<ShareItem> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            //TimeUnit.MILLISECONDS.sleep(450);
            Thread.sleep(450);
        }
        catch (InterruptedException e) {
            return;
        }
        while (true) {
            if (Thread.currentThread().isInterrupted()) return;
            try {
                ShareItem item = queue.take();
                System.out.format("Processing %s%n", item.toString());
            }
            catch (InterruptedException e) {
                return;
            }
        }

    }
}
