package com.javarush.task.task27.task2712;


import com.javarush.task.task27.task2712.kitchen.Cook;
import com.javarush.task.task27.task2712.kitchen.Order;
import com.javarush.task.task27.task2712.kitchen.Waiter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Restaurant {
    private static final int ORDER_CREATING_INTERVAL = 100;
    private static final LinkedBlockingQueue<Order> orderQueue = new LinkedBlockingQueue<>();
    private static final LinkedBlockingQueue<Order> readyOrders = new LinkedBlockingQueue<>();

    public static void main(String[] args) throws Exception {
        Cook cook = new Cook("Amigo");
        cook.setQueue(orderQueue);
        cook.setReadyOrders(readyOrders);
        Cook povar = new Cook("Повар_спрашивает_повара");
        povar.setQueue(orderQueue);
        povar.setReadyOrders(readyOrders);

        new Thread(cook).start();
        new Thread(povar).start();

        List<Tablet> tablets = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Tablet curTablet = new Tablet(i);
            curTablet.setQueue(orderQueue);
            tablets.add(curTablet);
        }

        Waiter waiter = new Waiter();
        waiter.setQueue(readyOrders);
        new Thread(waiter).start();
        Thread thread =
                new Thread(new RandomOrderGeneratorTask
                        (tablets, ORDER_CREATING_INTERVAL));
        thread.start();
        TimeUnit.SECONDS.sleep(3);
        RestaurantDate.getInstance().changeDay(1);
        TimeUnit.SECONDS.sleep(3);
        RestaurantDate.getInstance().changeDay(1);
        TimeUnit.SECONDS.sleep(3);
        thread.interrupt();
        RestaurantDate.getInstance().resetTime();


        DirectorTablet bossTablet = new DirectorTablet();

        bossTablet.printAdvertisementProfit();
        bossTablet.printCookWorkloading();
        bossTablet.printActiveVideoSet();
        bossTablet.printArchivedVideoSet();

    }
}
