package com.javarush.task.task32.task3211;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;

/* 
Целостность информации
*/

public class Solution {
    public static void main(String... args) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(new String("test string"));
        oos.flush();
        System.out.println(compareMD5(bos, "5a47d12a2e3f9fecf2d9ba1fd98152eb")); //true

    }

    public static boolean compareMD5(ByteArrayOutputStream byteArrayOutputStream, String md5) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        md.update(byteArrayOutputStream.toByteArray());
        BigInteger bInt = new BigInteger(1, md.digest());
        StringBuilder curMD5 = new StringBuilder(bInt.toString(16));
        while (curMD5.length() < 32) {
            curMD5.insert(0, 0);
        }
        return md5.equals(curMD5.toString());

        /*
        StringBuilder sb = new StringBuilder();
        byte[] mdBytes = md.digest();
        for (byte cur : mdBytes) {
            String curS = Integer.toHexString(0xff & cur);
            curS = (curS.length()) == 1 ? "0" + curS : curS;
            sb.append(curS);
        }
        return sb.toString().equals(md5);
        */
    }
}
