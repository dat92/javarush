package com.javarush.task.task21.task2103;

/* 
Все гениальное - просто!
*/
public class Solution {
    public static boolean calculate(boolean a, boolean b, boolean c, boolean d) {
        return c;
    }

    public static void main(String[] args) {

        for (int a=0; a < 2; a++) {
            for (int b=0; b < 2; b++) {
                for (int c=0; c < 2; c++) {
                    for(int d=0; d < 2; d++) {
                        System.out.print((a==1) + " ");
                        System.out.print((b==1) + " ");
                        System.out.print((c==1) + " ");
                        System.out.print((d==1) + " = ");
                        System.out.println(calculate((a==1), (b==1), (c==1), (d==1)));
                    }
                }
            }
        }
    }
}
