package com.javarush.task.task21.task2113;

import java.util.ArrayList;
import java.util.List;

public class Hippodrome {
    static Hippodrome game;
    private List<Horse> horses;

    public Hippodrome(List<Horse> horses) {
        this.horses = horses;
    }

    public List<Horse> getHorses() {
        return horses;
    }

    void run() {
        for (int i=0; i<100; i++) {
            move();
            print();

            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
    void move() {
        for (Horse curHorse : horses) {
            curHorse.move();
        }
    }
    void print() {
        for (Horse curHorse : horses) {
            curHorse.print();
        }
        for (int i=0; i<10; i++) {
            System.out.println();
        }
    }

    public Horse getWinner() {
        int maxI=0;
        double maxDistance = 0;
        for (int i=0; i<horses.size(); i++) {
            double curDistance = horses.get(i).getDistance();
            if (curDistance > maxDistance) {
                maxDistance = curDistance;
                maxI = i;
            }
        }
        return horses.get(maxI);
    }

    public void printWinner() {
        System.out.println("Winner is "+getWinner().getName() + "!");
    }

    public static void main(String[] args) {
        Horse first = new Horse("Sugar", 3, 0);
        Horse second = new Horse("Beast", 3, 0);
        Horse third = new Horse("Stinky", 3, 0);

        List<Horse> horsesList = new ArrayList<>();
        horsesList.add(first);
        horsesList.add(second);
        horsesList.add(third);

        game = new Hippodrome(horsesList);
        game.run();
        game.printWinner();
    }
}
