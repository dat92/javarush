package com.javarush.task.task21.task2105;

import java.util.HashSet;
import java.util.Set;

/* 
Исправить ошибку. Сравнение объектов
*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public boolean equals(Object o) {
        if (null == o) return false;
        if (o == this) return true;
        if (!(o instanceof Solution))
            return false;
        Solution n = (Solution) o;
        return (((null == first) && (null == n.first)) || (n.first.equals(first)))
                && (((null == last) && (null == n.last)) || (n.last.equals(last)));
    }

    public int hashCode() {
        int result=0;
        result += first == null ? 0 : first.hashCode();
        result += last == null ? 0 : last.hashCode();
        return 37 * result;
    }

    public static void main(String[] args) {
        Set<Solution> s = new HashSet<>();
        s.add(new Solution("Mickey", null));
        System.out.println(s.contains(new Solution("Mickey", null)));
    }
}
