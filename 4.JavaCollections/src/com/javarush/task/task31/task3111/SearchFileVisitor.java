package com.javarush.task.task31.task3111;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

public class SearchFileVisitor extends SimpleFileVisitor<Path> {
    private String partOfContent;
    private String partOfName;
    private int minSize = -1, maxSize = -1;
    private List<Path> foundFiles = new LinkedList<>();

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public void setPartOfName(String fileName) {
        partOfName = fileName;
    }

    public void setPartOfContent(String text) {
        partOfContent = text;
    }

    public List<Path> getFoundFiles() {
        return foundFiles;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

        //byte[] content = Files.readAllBytes(file); // размер файла: content.length

        if (isName(file) && isLarger(file) && isSmaller(file) && isContent(file)) {
            foundFiles.add(file);
        }

        return super.visitFile(file, attrs);
    }

    private boolean isName(Path file) {
        if (partOfName == null) return true;
        return (file.getFileName().toString().contains(partOfName));
    }

    private boolean isContent(Path file) throws IOException {
        if (partOfContent == null) return true;
        return Files.lines(file).anyMatch(e -> e.contains(partOfContent));
    }

    private boolean isSmaller(Path file) throws IOException {
        if (maxSize == -1) return true;
        return (maxSize > Files.size(file));
    }

    private boolean isLarger(Path file) throws IOException {
        if (minSize == -1) return true;
        return (minSize < Files.size(file));
    }
}
