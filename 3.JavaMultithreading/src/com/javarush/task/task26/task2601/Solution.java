package com.javarush.task.task26.task2601;

import java.util.Arrays;

/*
Почитать в инете про медиану выборки
*/
public class Solution {

    public static void main(String[] args) {

    }

    public static Integer[] sort(Integer[] array) {
        //implement logic here
        Arrays.sort(array);
        double median;
        if (array.length % 2 == 0) {
            median = ((double) array[array.length / 2] +
                    (double) array[array.length / 2 - 1]) / 2;
        } else {
            median = (double) array[array.length / 2];
        }

        for (int i=0; i<array.length; i++) {
            for (int j=0; j<(array.length-1)-i; j++) {
                int resultCompare = compareWithMedian(array[j], array[j+1], median);
                if (resultCompare < 0) continue;
                if (resultCompare == 0) {
                    if (array[j].compareTo(array[j+1]) <= 0) {
                        continue;
                    }
                }
                Integer tmp = array[j];
                array[j] = array[j+1];
                array[j+1] = tmp;
            }
        }
        return array;
    }

    private static int compareWithMedian(Integer val1, Integer val2, double median) {
        double residual1 = Math.abs(median - (double) val1);
        double residual2 = Math.abs(median - (double) val2);
        return (int) residual1 - (int) residual2;
    }
}
