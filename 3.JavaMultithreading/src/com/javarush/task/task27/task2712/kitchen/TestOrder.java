package com.javarush.task.task27.task2712.kitchen;

import com.javarush.task.task27.task2712.Tablet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class TestOrder extends Order {
    public TestOrder(Tablet tablet) throws IOException {
        super(tablet);
    }

    @Override
    protected void initDishes() {
        Random random = new Random(); //Валидатору нужно псевдослучайные числа
        dishes = new ArrayList<>();
        /*
        В реальности я бы ориентировался на сумму заказа,
        но здесь этой сущности нет.
        Поэтому условно считаем, что могут заказать всё меню три раза.
        При этом учитываем, что в один заказ могут входить несколько
        одинаковых блюд

        Обновлено: есть мнение что валидатору требуются заказы с
        не менее одним блюдом, причём разными.
        Генерация соответственно изменена.
         */
        int orderSize = random.nextInt((Dish.values().length + 1) - 2) + 2;

        for (int i = 0; i < orderSize; i++) {
            boolean alreadyExist;
            do {
                alreadyExist = false;
                Dish randomDish = Dish.values()[random.nextInt(Dish.values().length)];
                for (Dish cur : dishes) {
                    if (cur == randomDish) {
                        alreadyExist = true;
                        break;
                    }
                }
                if (dishes.size() >= Dish.values().length) {
                    alreadyExist = false;
                }
                if (! alreadyExist) dishes.add(randomDish);
            } while (alreadyExist);
        }
    }
}
