package com.javarush.task.task31.task3109;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/* 
Читаем конфиги
*/
public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        Properties properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/properties.xml");
        properties.list(System.out);

        properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/properties.txt");
        properties.list(System.out);

        properties = solution.getProperties("4.JavaCollections/src/com/javarush/task/task31/task3109/notExists");
        properties.list(System.out);
    }

    public Properties getProperties(String fileName) {
        Properties properties = new Properties();
        Path file = Paths.get(fileName);
        if (Files.notExists(file)) return properties;

        boolean fileIsXML;
        try (BufferedReader reader = new BufferedReader(new FileReader(file.toFile()))) {
            fileIsXML = reader.readLine().startsWith("<?xml version=");
        } catch (IOException ex) {
            return properties;
        }


        try (FileInputStream fis = new FileInputStream(file.toFile())) {
            if (fileIsXML) properties.loadFromXML(fis);
            else properties.load(fis);
        } catch (IOException ex) {
            return properties;
        }

        return properties;
    }
}
