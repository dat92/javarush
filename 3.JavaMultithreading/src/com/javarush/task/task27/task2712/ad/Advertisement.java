package com.javarush.task.task27.task2712.ad;

public class Advertisement {
    private Object content; //Видео
    private String name; //Название
    private long initialAmount; //Начальная сумма, стоимость рекламы в копейках
    private int hits; //Кол-во оплаченных показов
    private int duration; //Продолжительность в секундах

    private long amountPerOneDisplaying;

    public Advertisement(Object content, String name, long initialAmount, int hits, int duration) {
        this.content = content;
        this.name = name;
        this.initialAmount = initialAmount;
        this.hits = hits;
        this.duration = duration;

        amountPerOneDisplaying = hits == 0 ? -1 : initialAmount / hits;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public long getAmountPerOneDisplaying() {
        return amountPerOneDisplaying;
    }

    public int getHits() {
        return hits;
    }

    public void revalidate() {
        if (hits < 1) throw new UnsupportedOperationException();
        hits--;
    }

    public long getAmountPerOneSecond() {
        return (Math.round(amountPerOneDisplaying * 1000 / duration));
    }
}
