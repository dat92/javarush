package com.javarush.task.task36.task3602;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;

/* 
Найти класс по описанию
*/
public class Solution {
    public static void main(String[] args) {
        System.out.println(getExpectedClass());
    }

    public static Class getExpectedClass() {
        Class cl;
        try {
            cl = Class.forName("java.util.Collections");
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        for (Class<?> curCl : cl.getDeclaredClasses()) {
            if (! Modifier.isPrivate(curCl.getModifiers())) continue;
            if (! Modifier.isStatic(curCl.getModifiers())) continue;
            if (! List.class.isAssignableFrom(curCl)) continue;
            try {
                Method met = curCl.getMethod("get", int.class);
                met.setAccessible(true);

                Constructor constructor = null;
                for (Constructor<?> curConstr : curCl.getDeclaredConstructors()) {
                    if (curConstr.getParameterCount() != 0) continue;
                    constructor = curConstr;
                    break;
                }
                if (constructor == null) continue;


                constructor.setAccessible(true);
                met.invoke(constructor.newInstance(), 0);
            }
            catch (NoSuchMethodException | IllegalAccessException | InstantiationException e) {
                e.printStackTrace();
            }
            catch (InvocationTargetException e) {
                if (e.getCause().toString().contains(IndexOutOfBoundsException.class.getSimpleName())) {
                    return curCl;
                }
            }
        }
        return null;
    }
}
