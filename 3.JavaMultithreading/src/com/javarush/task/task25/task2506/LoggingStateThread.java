package com.javarush.task.task25.task2506;

public class LoggingStateThread extends Thread {
    private Thread watchThread;
    public LoggingStateThread(Thread thread) {
        watchThread = thread;
    }

    public void run() {
        State curState, lastState = null;
        do {
            curState = watchThread.getState();
            if (lastState != curState) System.out.println(curState);
            lastState = curState;
        } while (curState != State.TERMINATED);
    }
}
