package com.javarush.task.task31.task3101;

import java.io.*;
import java.util.*;

/*
Проход по дереву файлов
*/
public class Solution {

    TreeMap<String, File> fileList = new TreeMap<>();

    private void getFileContest(File folder) {
        if (null == folder) return;
        for (File curFile : folder.listFiles()) {
            if (curFile.isDirectory()) {
                getFileContest(curFile);
                continue;
            }
            if (curFile.getName().endsWith(".txt") && curFile.length() <= 50) {
                fileList.put(curFile.getName() + "..." + curFile.getAbsolutePath()
                        , curFile);
            }
        }


    }

    private void writeMapToFile(FileOutputStream fos) throws IOException {
        for (Map.Entry<String, File> entry : fileList.entrySet()) {
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(entry.getValue())))) {
                while (reader.ready()) {
                    fos.write(reader.readLine().getBytes());
                }
                fos.write('\n');
            }
        }
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("Incorrect parameters");
            return;
        }

        File folder = new File(args[0]);

        //На проверки по параметрам ругается валидатор

        File inputFile = new File(args[1]);
        File resultFile = new File(inputFile.getParent()
                + "/" + "allFilesContent.txt");
        FileUtils.renameFile(inputFile, resultFile);

        Solution solution = new Solution();
        solution.getFileContest(folder);
        try (FileOutputStream fos = new FileOutputStream(resultFile)) {
            solution.writeMapToFile(fos);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
