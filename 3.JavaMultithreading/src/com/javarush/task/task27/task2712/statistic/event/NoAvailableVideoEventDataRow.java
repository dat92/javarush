package com.javarush.task.task27.task2712.statistic.event;

import com.javarush.task.task27.task2712.RestaurantDate;

import java.util.Date;

public class NoAvailableVideoEventDataRow implements EventDataRow {
    private Date currentDate;
    private int totalDuration;

    public NoAvailableVideoEventDataRow(int totalDuration) { //время приготовления в сек
        currentDate = new Date(RestaurantDate.getInstance().getTime());
        this.totalDuration = totalDuration;
    }

    @Override
    public EventType getType() {
        return EventType.NO_AVAILABLE_VIDEO;
    }

    @Override
    public Date getDate() {
        return currentDate;
    }

    @Override
    public int getTime() {
        return totalDuration;
    }
}
