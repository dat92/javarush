package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<LineItem>();

    public static void main(String[] args) {
        String fileName1, fileName2;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            fileName1 = reader.readLine();
            fileName2 = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        compareFiles(fileName1, fileName2);

        for (LineItem cur : lines) {
            System.out.printf("%s = %s\n", cur.type, cur.line);
        }
    }

    private static void compareFiles(String fileName1, String fileName2) {
        try (BufferedReader reader1 = new BufferedReader(new FileReader(fileName1));
             BufferedReader reader2 = new BufferedReader(new FileReader(fileName2))) {

            String file1String = reader1.readLine();
            String file2String = reader2.readLine();
            String file1NextString = reader1.readLine();
            String file2NextString = reader2.readLine();

            while ((file1String.length() !=0) || (file2String.length() !=0)) {
                if (file1String.equalsIgnoreCase(file2String)) {
                    lines.add(new LineItem(Type.SAME, file1String));
                    file1String = file1NextString;
                    file2String = file2NextString;
                    file1NextString = readNextLine(reader1);
                    file2NextString = readNextLine(reader2);
                } else if (file2String.equalsIgnoreCase(file1NextString)) {
                    lines.add(new LineItem(Type.REMOVED, file1String));
                    file1String = file1NextString;
                    file1NextString = readNextLine(reader1);
                } else if (file1String.equalsIgnoreCase(file2NextString)) {
                    lines.add(new LineItem(Type.ADDED, file2String));
                    file2String = file2NextString;
                    file2NextString = readNextLine(reader2);
                } else {
                    //Противоречит условию: операции удаления и добавления не могут идти подряд
                    System.err.println("Incorrect data format");
                    System.err.printf("f1s=\"%s\"\nf1ns=\"%s\"\nf2s=\"%s\"\nf2ns\"%s\"",
                            file1String, file1NextString, file2String, file2NextString);
                    return;
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private static String readNextLine(BufferedReader reader) throws IOException {
        if (!reader.ready()) return "";
        return reader.readLine();
    }


    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}
