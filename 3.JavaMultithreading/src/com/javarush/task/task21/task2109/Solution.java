package com.javarush.task.task21.task2109;

/* 
Запретить клонирование
*/
public class Solution {
    public static class A implements Cloneable {
        private int i;
        private int j;

        public A(int i, int j) {
            this.i = i;
            this.j = j;
        }

        public int getI() {
            return i;
        }

        public int getJ() {
            return j;
        }
    }

    public static class B extends A {
        private String name;

        protected B clone() throws CloneNotSupportedException {
            throw new CloneNotSupportedException();
        }

        public B(int i, int j, String name) {
            super(i, j);
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public static class C extends B implements Cloneable {
        protected C clone() throws CloneNotSupportedException {
            C clone = new C(getI(), getJ(), getName());
            return clone;
        }

        public C(int i, int j, String name) {
            super(i, j, name);
        }

        public boolean equals(Object n) {
            if (null == n) return false;
            if (n == this) return true;
            if (!(n instanceof C)) return false;
            C c = (C) n;
            if (null == getName() ? c.getName() != null : c.getName().equals(getName())) return false;
            if (getI() != c.getI()) return false;
            if (getJ() != c.getJ()) return false;
            return true;
        }

        public int hashCode() {
            int result = getI();
            result += getJ();
            result += getName().hashCode();
            return 37 * result;
        }
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        C source = new C(25, 50, "test");
        C clone = source.clone();
        System.out.println(source);
        System.out.println(clone);
        System.out.println(source.getName() + " " +
                source.getI() + " " +
                source.getJ());
        System.out.println(clone.getName() + " " +
                clone.getI() + " " +
                clone.getJ());
    }
}
