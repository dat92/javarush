package com.javarush.task.task20.task2002;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
Читаем и пишем в файл: JavaRush
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or adjust outputStream/inputStream according to your file's actual location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File yourFile = File.createTempFile("task2002_", null);
            OutputStream outputStream = new FileOutputStream(yourFile);
            InputStream inputStream = new FileInputStream(yourFile);

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");

            User vasya = new User();
            vasya.setFirstName("Vasya");
            vasya.setLastName("Pupkin");
            vasya.setMale(true);
            vasya.setBirthDate(dateFormat.parse("1987.11.26"));
            vasya.setCountry(User.Country.RUSSIA);
            javaRush.users.add(vasya);

            User pupkenko = new User();
            pupkenko.setFirstName("Petro");
            pupkenko.setLastName("Pupkenko");
            pupkenko.setMale(true);
            pupkenko.setBirthDate(dateFormat.parse("1989.6.23"));
            pupkenko.setCountry(User.Country.UKRAINE);
            javaRush.users.add(pupkenko);

            User doro = new User();
            doro.setFirstName("Doro");
            doro.setLastName("Pesch");
            doro.setMale(false);
            doro.setBirthDate(dateFormat.parse("1964.6.3"));
            doro.setCountry(User.Country.OTHER);
            javaRush.users.add(doro);


            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //here check that the codeGym object is equal to the loadedObject object - проверьте тут, что javaRush и loadedObject равны
            System.out.println(loadedObject.equals(javaRush));
            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Oops, something is wrong with my file");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Oops, something is wrong with the save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            if (users.size() == 0) {
                outputStream.write(48); //ZERO (ASCII) - not users
                outputStream.flush();
                return;
            }

            outputStream.write(49); //Have users
            for (User curUser : users) {
                outputStream.write(((curUser.getFirstName()) + "\n").getBytes());
                outputStream.write(((curUser.getLastName()) + "\n").getBytes());
                outputStream.write(((curUser.isMale()?"m":"f") + "\n").getBytes());
                outputStream.write(((curUser.getBirthDate().getTime()) + "\n").getBytes());
                outputStream.write(((curUser.getCountry().getDisplayName()) + "\n").getBytes());
            }
            outputStream.write(3);
            outputStream.flush();

        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            if (inputStream.read() == 0) return; //Not users
            while (inputStream.available() > 0) {
                String firstName = readString(inputStream);
                if (firstName.isEmpty()) return;

                String lastName = readString(inputStream);
                if (lastName.isEmpty()) return;

                String gender = readString(inputStream);
                boolean isMale;
                switch (gender) {
                    case "m":
                        isMale = true;
                        break;
                    case "f":
                        isMale = false;
                        break;
                    default:
                        return;
                }

                String strDate = readString(inputStream);
                if (strDate.isEmpty()) return;
                Date birthday = new Date(Long.parseLong(strDate));

                String strCountry = readString(inputStream);
                if (strCountry.isEmpty()) return;
                User.Country country = User.Country.valueOf(strCountry.toUpperCase());

                User user = new User();
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setMale(isMale);
                user.setBirthDate(birthday);
                user.setCountry(country);
                users.add(user);
            }
        }

        private String readString(InputStream inputStream) throws Exception {
            StringBuilder sb = new StringBuilder();
            while (inputStream.available() > 0) {
                int cur = inputStream.read();
                if (cur == 10 || cur == 3) break; //LF or EOF
                sb.append((char) cur);
            }
            return sb.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}
