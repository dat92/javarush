package com.javarush.task.task27.task2712.ad;

import com.javarush.task.task27.task2712.ConsoleHelper;
import com.javarush.task.task27.task2712.statistic.StatisticManager;
import com.javarush.task.task27.task2712.statistic.event.VideoSelectedEventDataRow;

import java.util.*;

public class AdvertisementManager {
    private final AdvertisementStorage storage = AdvertisementStorage.getInstance();
    private int timeSeconds;

    public AdvertisementManager(int timeSeconds) {
        this.timeSeconds = timeSeconds;
    }

    public void processVideos() throws NoVideoAvailableException {
        if (storage.list().isEmpty()) throw new NoVideoAvailableException();
        AdvertisementRow row = selectingOfAdvertisements();
        StatisticManager.getInstance().register(
                new VideoSelectedEventDataRow(
                        row.getList(),
                        row.getTotalPrice(),
                        row.getTotalDuration()
                )
        );
        List<Advertisement> advertisements = row.getList();


        Comparator<Advertisement> compareByPrice = new Comparator<Advertisement>() {
            @Override
            public int compare(Advertisement advertisement, Advertisement t1) {
                int amount =
                        (int) (t1.getAmountPerOneDisplaying() - advertisement.getAmountPerOneDisplaying());
                if (amount != 0) return amount;
                return (int) (advertisement.getAmountPerOneSecond() - t1.getAmountPerOneSecond());
            }
        };

        Collections.sort(advertisements, compareByPrice);

        for (Advertisement cur: advertisements) {
            cur.revalidate();
            String message = String.format("%s is displaying... %d, %d%n",
                    cur.getName(),
                    cur.getAmountPerOneDisplaying(),
                    cur.getAmountPerOneSecond());
            ConsoleHelper.writeMessage(message);
        }
    }

    private class AdvertisementRow {
        private final boolean valid;
        private final long totalPrice;
        private final int totalDuration;
        private final int clipCount;

        private List<Advertisement> list;

        public AdvertisementRow(List<Advertisement> list) {
            this.list = list;
            clipCount = list.size();

            long price = 0;
            int duration = 0;
            for (Advertisement curAdvertisement : list) {
                if (curAdvertisement.getHits() < 1) {
                    valid = false;
                    totalPrice = -1;
                    totalDuration = -1;
                    return;
                }
                price += curAdvertisement.getAmountPerOneDisplaying();
                duration += curAdvertisement.getDuration();
            }
            totalPrice = price;
            totalDuration = duration;
            valid = totalDuration <= timeSeconds;
        }

        public List<Advertisement> getList() {
            return list;
        }

        public boolean isValid() {
            return valid;
        }

        public long getTotalPrice() {
            return totalPrice;
        }

        public int getTotalDuration() {
            return totalDuration;
        }

        public int getClipCount() {
            return clipCount;
        }
    }

    private AdvertisementRow selectingOfAdvertisements() {
        List<AdvertisementRow> combinations = new ArrayList<>();
        getCombinations(combinations, new ArrayList<>(), 0);
        combinations = prefiltration(combinations);
        if (combinations.size() == 1) return combinations.get(0);

        filteringByDuration(combinations);
        if (combinations.size() == 1) return combinations.get(0);

        filteringByClipCount(combinations);
        return combinations.get(0);
    }

    private void getCombinations(List<AdvertisementRow> result,
                                 List<Advertisement> prefix, int digit) {
        if (digit > storage.list().size()) return;

        result.add(new AdvertisementRow(prefix));

        for (int i = digit; i < storage.list().size(); i++) {
            List<Advertisement> newPref = new LinkedList<>(prefix);
            newPref.add(storage.list().get(i));
            getCombinations(result, newPref, ++digit);
        }
    }


    private void filteringByClipCount(List<AdvertisementRow> list) {
        int minClip = Integer.MAX_VALUE;

        for (AdvertisementRow curRow : list) {
            if (curRow.getClipCount() < minClip) minClip = curRow.getClipCount();
        }

        for (Iterator<AdvertisementRow> it = list.iterator(); it.hasNext();) {
            if (it.next().getClipCount() > minClip) it.remove();
        }
    }

    private void filteringByDuration(List<AdvertisementRow> list) {
        int maxDuration = 0;
        for (AdvertisementRow curRow : list) {
            int curDuration = curRow.getTotalDuration();
            if (curDuration > maxDuration) maxDuration = curDuration;
        }

        for (Iterator<AdvertisementRow> it = list.iterator(); it.hasNext();) {
            if ((it.next().getTotalDuration()) < maxDuration) {
                it.remove();
            }
        }
    }

    private List<AdvertisementRow> prefiltration(List<AdvertisementRow> list) {
        long maxPrice = 0;
        Map<AdvertisementRow, Long> combPrice = new HashMap<>();
        for (AdvertisementRow curRow : list) {
            if (!curRow.isValid()) continue;
            combPrice.put(curRow, curRow.getTotalPrice());
            if (maxPrice < curRow.getTotalPrice()) {
                maxPrice = curRow.getTotalPrice();
            }
        }

        List<AdvertisementRow> result = new ArrayList<>();
        for (Map.Entry<AdvertisementRow, Long> entry : combPrice.entrySet()) {
            if (entry.getValue() == maxPrice) result.add(entry.getKey());
        }
        return result;
    }

}
