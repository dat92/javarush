package com.javarush.task.task31.task3112;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.*;

/* 
Загрузчик файлов
*/
public class Solution {

    public static void main(String[] args) throws IOException {
        Path passwords = downloadFile("https://javarush.ru/testdata/secretPasswords.txt",
                Paths.get("/tmp/Test"));

        for (String line : Files.readAllLines(passwords)) {
            System.out.println(line);
        }
    }

    public static Path downloadFile(String urlString, Path downloadDirectory) throws IOException {
        // implement this method
        URL url = new URL(urlString);

        InputStream stream = url.openStream();
        Path tmpFile = Files.createTempFile("task3112_part-", ".tmp");
        Files.copy(stream, tmpFile, StandardCopyOption.REPLACE_EXISTING);
        Path resultFile = downloadDirectory.resolve(
                Paths.get(url.getFile().substring(url.getFile().lastIndexOf('/') + 1)));
        Files.move(tmpFile, resultFile, StandardCopyOption.REPLACE_EXISTING);
        return resultFile;
    }
}
