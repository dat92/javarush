package com.javarush.task.task31.task3105;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/* 
Добавление файла в архив
*/
public class Solution {
    private static final byte[] BUFFER = new byte[4096 * 1024];

    public static void main(String[] args) throws IOException {
        if (args.length != 2) return;

        Map<ZipEntry, byte[]> archiveContent = new HashMap<>();
        try (ZipInputStream zis =
                     new ZipInputStream(new FileInputStream(args[1]))) {
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                if (entry.getName().substring(entry.getName().lastIndexOf(File.separatorChar) + 1).
                        equals(args[0].substring(args[0].lastIndexOf(File.separatorChar)+1))) {
                    zis.closeEntry();
                    continue;
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int read;
                while ((read = zis.read(BUFFER)) != -1) {
                    baos.write(BUFFER, 0, read);
                }
                archiveContent.put(entry, baos.toByteArray());
                zis.closeEntry();
            }
        }

        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(args[1]))) {
            zos.putNextEntry(new ZipEntry("new/" +
                    args[0].substring(args[0].lastIndexOf(File.separatorChar) + 1)));
            Path newFile = Paths.get(args[0]);
            Files.copy(newFile, zos);

            for (Map.Entry<ZipEntry, byte[]> mapEntry : archiveContent.entrySet()) {
                ZipEntry newEntry = new ZipEntry(mapEntry.getKey());
                newEntry.setCompressedSize(-1);

                zos.putNextEntry(newEntry);
                if (!mapEntry.getKey().isDirectory()) {
                    ByteArrayInputStream bais =
                            new ByteArrayInputStream(mapEntry.getValue());

                    int read;
                    while ((read = bais.read(BUFFER)) != -1) {
                        zos.write(BUFFER, 0, read);
                    }
                }
                zos.closeEntry();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
