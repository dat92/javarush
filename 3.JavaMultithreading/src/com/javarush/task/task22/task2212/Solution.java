package com.javarush.task.task22.task2212;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Проверка номера телефона
*/
public class Solution {
    public static boolean checkTelNumber(String telNumber) {
        if (null == telNumber || telNumber.isEmpty()) return false;

        //Не придумал как с помощью единой регулярки проверить кол-во цифр по условию :(
        int telnumbLen=0;
        if (telNumber.matches("^\\+([\\d-()])*")) telnumbLen = 12;
        else if (telNumber.matches("^[(\\d]([\\d-()])*")) telnumbLen = 10;
        else return false; //Сразу отбраковываем если где-то встречаются не те символы

        String testLenStr = telNumber.replaceAll("\\D+","");
        if (testLenStr.length() != telnumbLen) {
            System.out.printf("%d%s%d%n", testLenStr.length(), "!=", telnumbLen);
            return false;
        }

        /*
        "Портянка" делает следующее:
        Проверяем что строка может начинаться с '+'
        Далее (или с начала) могут идти цифры(цифра)
        Далее (или с начала) может идти следующая конструкция по условию
            (ddd)
            при этом это часть положительного Lookahead, которая следует
            строго до символа '-' или цифры, т.к. '-' может и не быть
            Вся эта конструкция может быть только один раз (?)
        Далее могут идти числа
        Далее может быть один символ '-'
        Далее опять цифры (т.к. по условию '-' не могут идти подряд)
        Далее опять может быть символ '-'
            Т.е. может быть следующее: ddddd-dd-dddd
        Наконец, последний символ строго цифра.
        Валидатор принял, протестировано на что хватило терпения.
         */

        Pattern p = Pattern.compile("^\\+?\\d*(\\(\\d{3}\\)(?=(-)|(\\d)))?\\d*-?\\d*-?\\d*\\d$");
        Matcher m = p.matcher(telNumber);
        return m.matches();
    }

    public static void main(String[] args) {
        String[] numbers = {
                "+380501234567",
                "+38(050)1234567",
                "+38050123-45-67",
                "050123-4567",
                "",
                "+(123)45678-91-11",
                "(123)4567890",
                "1234567890",
                "1(234)56-78-90",
                "8(123)99-55-33",
                "+7(136)444-565-33",
                null,
                "+38)050(1234567",
                "+38(050)1-23-45-6-7",
                "050ххх4567",
                "050123456",
                "(0)501234567",
                "",
                "+7(434)55-43-54-44",
                "1(2345)67890",
                "abc",
                "012345678999"
        };
        for (String cur : numbers) System.out.printf("%20s    %b%n", cur, checkTelNumber(cur));
    }
}
