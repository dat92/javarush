package com.javarush.task.task37.task3711;

public class Computer {
    private CPU cpu = new CPU();
    private Memory memory = new Memory();
    private HardDrive hdd = new HardDrive();

    public void run() {
        cpu.calculate();
        memory.allocate();
        hdd.storeData();
    }
}
