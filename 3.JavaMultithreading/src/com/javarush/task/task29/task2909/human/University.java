package com.javarush.task.task29.task2909.human;

import java.util.ArrayList;
import java.util.List;

public class University {

    private List<Student> students = new ArrayList<>();

    public University(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student getStudentWithAverageGrade(double grade) {
        for (Student cur : students) {
            if (cur.getAverageGrade() == grade) return cur;
        }
        return null;
    }

    public Student getStudentWithMaxAverageGrade() {
        Student maxGradeStudent = students.get(0);
        for (Student cur : students) {
            if (cur.getAverageGrade() > maxGradeStudent.getAverageGrade()) {
                maxGradeStudent = cur;
            }
        }
        return maxGradeStudent;
    }

    public Student getStudentWithMinAverageGrade() {
        Student minGradeStudent = students.get(0);
        for (Student cur : students) {
            if (cur.getAverageGrade() < minGradeStudent.getAverageGrade()) {
                minGradeStudent = cur;
            }
        }
        return minGradeStudent;
    }

    public void expel(Student student) {
        students.remove(student);
    }
}