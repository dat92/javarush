package com.javarush.task.task22.task2209;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/*
Составить цепочку слов
*/
public class Solution {
    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String fileName = reader.readLine();
            Files.lines(Paths.get(fileName), StandardCharsets.UTF_8)
                    .forEach(s -> words.addAll(Arrays.asList(s.split(" "))));
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
        //...
        StringBuilder result = getLine(words.toArray(new String[0]));
        //Менял и на print - не влияет на валидатор
        System.out.println(result.toString());
    }

    private static List<String> getChain(List<String> wordList) {
        char lastChar=wordList.get(0).toLowerCase().charAt(wordList.get(0).length()-1);
        int ListLen=0;
        for (int i=0; i<wordList.size()-1; i++) {
            for (int j=i+1; j<wordList.size(); j++) {
                if (wordList.get(j).toLowerCase().charAt(0) == lastChar) {
                    String tmp = wordList.get(j);
                    wordList.remove(j);
                    wordList.add(i+1, tmp);
                    lastChar = wordList.get(i+1).toLowerCase().charAt(wordList.get(i+1).length()-1);
                    ListLen++;
                    break;
                }
            }
        }
        /*
        То из чего не получилось склеить данную цепочку
        не включаем в результат.
        (Выяснено опытным путём)
         */
        return wordList.subList(0, ListLen+1);
    }

    private static List<String> selectChain(String... words) {
        //Получаем все (или почти все) возможные цепочки слов

        /*
        Образцовая пооследовательность слов - т.к. оригинал
        менять не можем по условию
         */
        List<String> wordList = new LinkedList<>(Arrays.asList(words));
        /*
        Сортируем в алфавитном порядке
        Работает и без этого (на валидатор не влияет),
        но не совсем ясно по условию: результат должен
        быть в алфавитном порядке или нет
        Скорее всего не нужно, но повторно проверить валидатором не могу
         */
        Collections.sort(wordList);
        List<List> chains = new ArrayList<>();

        for (int i=0; i<words.length; i++) {
            //Клонируем образцовую пооследовательность
            LinkedList<String> cloneList =
                    (LinkedList<String>) ((LinkedList<String>) wordList).clone();
            /*
            Меняем первое слово (влияет на работу ф-ии getChain):
            от первого слова происходит "сортировка" и размер цепочки
            получается разный
            */
            String tmp = cloneList.get(i);
            cloneList.remove(i);
            cloneList.add(0, tmp);
            List<String> cur = getChain(cloneList);
            //Получили максимально возможную и полную цепочку
            if (cur.size() == wordList.size()) return cur;
            else chains.add(cur);
        }

        int maxIndex = -1;
        int maxLen = 0;
        for (int i=0; i<chains.size(); i++) {
            int curLen = chains.get(i).size();
            if (curLen > maxLen) {
                maxLen = curLen;
                maxIndex = i;
            }
        }
        return chains.get(maxIndex);
    }

    public static StringBuilder getLine(String... words) {
        StringBuilder sb = new StringBuilder();
        if (null == words || (words.length == 0)) return sb;
        List<String> wordsList = selectChain(words);
        wordsList.forEach(s -> sb.append(s).append(" "));
        // Удаляем пробел в конце (На валидатор не влияет?)
        sb.delete(sb.length()-1, sb.length());
        return sb;
    }
}