package com.javarush.task.task27.task2712.kitchen;


import com.javarush.task.task27.task2712.ConsoleHelper;
import com.javarush.task.task27.task2712.Tablet;

import java.io.IOException;
import java.util.List;

public class Order {

    private final Tablet tablet;
    protected List<Dish> dishes;
    protected Cook cook;


    protected boolean empty = true;

    public Order(Tablet tablet) throws IOException {
        this.tablet = tablet;
        initDishes();
        if (null != dishes && dishes.size() > 0) empty = false;
    }

    protected void initDishes() throws IOException {
        dishes = ConsoleHelper.getAllDishesForOrder();
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setCook(Cook cook) {
        this.cook = cook;
    }

    public Cook getCook() {
        return cook;
    }

    public Tablet getTablet() {
        return tablet;
    }

    public int getTotalCookingTime() {
        int count=0;
        for (Dish cur : dishes) {
            count += cur.getDuration();
        }
        return count;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    @Override
    public String toString() {
        if (dishes == null) return "";
        StringBuilder sb = new StringBuilder();
        sb.append("Your order: [");
        dishes.forEach(x -> sb.append(x).append(", "));
        sb.delete(sb.length() - 2, sb.length());
        sb.append("] of ").append(tablet);
        return sb.toString();
    }
}
