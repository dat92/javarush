package com.javarush.task.task27.task2712.ad;

import java.util.ArrayList;
import java.util.List;


public class StatisticAdvertisementManager {

    private static StatisticAdvertisementManager ourInstance =
            new StatisticAdvertisementManager();

    private AdvertisementStorage storage = AdvertisementStorage.getInstance();

    public static StatisticAdvertisementManager getInstance() {
        return ourInstance;
    }

    private StatisticAdvertisementManager() {
    }

    public List<Advertisement> getAdvertisementsByStatus(boolean active) {
        List<Advertisement> advertisements = new ArrayList<>();
        for (Advertisement curAdvertisement : storage.list()) {
            if ((curAdvertisement.getHits() > 0 && active) ||
                    (curAdvertisement.getHits() < 1 && !active)) {
                advertisements.add(curAdvertisement);
            }
        }
        return advertisements;
    }
}
