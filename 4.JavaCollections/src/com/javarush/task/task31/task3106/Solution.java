package com.javarush.task.task31.task3106;

/* 
Разархивируем файл
*/

import java.io.*;
import java.util.*;
import java.util.zip.ZipInputStream;

public class Solution {

    class InputStreamSortedEnumeration implements Enumeration<FileInputStream> {
        private Vector<FileInputStream> files;
        private final boolean validVector;

        public InputStreamSortedEnumeration(String[] fileNames) {
            List<String> listFiles = new LinkedList<>(Arrays.asList(fileNames));
            try {
                listFiles.sort((f1, f2) -> {
                    int partF1 = Integer.parseInt(
                            f1.substring(f1.lastIndexOf(".") + 1));

                    int partF2 = Integer.parseInt(
                            f2.substring(f2.lastIndexOf(".") + 1));
                    return partF1 - partF2;
                });
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                validVector = false;
                return;
            }

            files = new Vector<>(listFiles.size());
            for (String curName : listFiles) {

                try {
                    files.add(new FileInputStream(curName));
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                    validVector = false;
                    return;
                }
            }
            validVector = true;
        }

        @Override
        public boolean hasMoreElements() {
            return validVector && !files.isEmpty();
        }

        @Override
        public FileInputStream nextElement() {
            if (!validVector) {
                throw new IllegalStateException();
            }
            if (files.size() < 1) {
                throw new NoSuchElementException();
            }
            FileInputStream cur = files.get(0);
            files.removeElementAt(0);
            return cur;
        }
    }

    public static void main(String[] args) {
        if (args.length < 2) return;
        Solution.InputStreamSortedEnumeration isse =
                new Solution().new InputStreamSortedEnumeration(
                        Arrays.copyOfRange(args, 1, args.length));

        try (ZipInputStream zis =
                     new ZipInputStream(new SequenceInputStream(isse))) {
            while ((zis.getNextEntry()) != null) {
                try (OutputStream os = new BufferedOutputStream(
                        new FileOutputStream(args[0]))) {
                    byte[] buffer = new byte[1024];
                    int read;
                    while ((read = zis.read(buffer)) != -1) {
                        os.write(buffer, 0, read);
                    }
                    os.flush();
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
