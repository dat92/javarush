package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;

/* 
Построй дерево(1)
*/
public class CustomTree extends AbstractList<String>
        implements Cloneable, Serializable {

    Entry<String> root;

    private int size;

    public CustomTree() {
        root = new Entry<>("0");
        size = 0;
    }

    @Override
    public boolean add(String s) {
        WalkTree walkTree = new WalkTree();
        while (walkTree.hasNext()) {
            Entry<String> current = walkTree.getNext();
            if (! current.isAvailableToAddChildren()) continue;
            if (current.availableToAddLeftChildren) {
                Entry<String> newEntry = new Entry<>(s);
                current.leftChild = newEntry;
                newEntry.parent = current;
                current.availableToAddLeftChildren = false;
                size++;
                return true;
            } else if (current.availableToAddRightChildren) {
                Entry<String> newEntry = new Entry<>(s);
                current.rightChild = newEntry;
                newEntry.parent = current;
                current.availableToAddRightChildren = false;
                size++;
                return true;
            }
        }
        //Ни один лист не может иметь потомков
        fixLeaves();
        return add(s);
    }

    private void fixLeaves() {
        WalkTree walkTree = new WalkTree();

        while (walkTree.hasNext()) {
            Entry<String> current = walkTree.getNext();
            if (! current.availableToAddLeftChildren && current.leftChild == null) {
                current.availableToAddLeftChildren = true;
            }
            if (! current.isAvailableToAddChildren() && current.rightChild == null) {
                current.availableToAddRightChildren = true;
            }
        }
    }

    @Override
    public boolean remove(Object o) {
        if (! (o instanceof String)) throw new UnsupportedOperationException();

        String name = (String) o;
        Entry<String> foundEntry = findFirstEntry(name);
        if (foundEntry == null) return false;
        Entry<String> parent = foundEntry.parent;
        if (parent.leftChild != null &&
                parent.leftChild.elementName.equalsIgnoreCase(name)) {
            parent.leftChild = null;
            updateSize();
            return true;
        } else if (parent.rightChild != null &&
                parent.rightChild.elementName.equalsIgnoreCase(name)) {
            parent.rightChild = null;
            updateSize();
            return true;
        }
        return false;
    }

    @Override
    public String get(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int i, String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String set(int i, String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String remove(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> subList(int i, int i1) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange(int i, int i1) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int i, Collection<? extends String> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return size;
    }

    private Entry<String> findFirstEntry(String name) {
        WalkTree walkTree = new WalkTree();
        while (walkTree.hasNext()) {
            Entry<String> current = walkTree.getNext();
            if (name.equalsIgnoreCase(current.elementName)) {
                return current;
            }
        }
        return null;
    }

    public String getParent(String s) {
        Entry<String> foundEntry = findFirstEntry(s);
        if (foundEntry != null) return foundEntry.parent.elementName;
        else return null;
    }

    private void updateSize() {
        WalkTree walkTree = new WalkTree();
        int newSize = 0;
        while (walkTree.hasNext()) {
            Entry<String> cur = walkTree.getNext();
            newSize++;
        }
        size = (newSize - 1); //Не учитываем корень
    }

    public void printTree() {
        Queue<Entry<String>> curLevel = new LinkedList<>();
        Queue<Entry<String>> nextLevel = new LinkedList<>();

        curLevel.add(root);

        while (!curLevel.isEmpty()) {
            Entry<String> curEntry = curLevel.poll();
            if (curEntry.leftChild != null) nextLevel.offer(curEntry.leftChild);
            if (curEntry.rightChild != null) nextLevel.offer(curEntry.rightChild);
            System.out.print(curEntry.elementName + " ");
            if (curLevel.isEmpty()) {
                System.out.println();
                curLevel = nextLevel;
                nextLevel = new LinkedList<>();
            }
        }
    }

    private class WalkTree {
        private Queue<Entry<String>> queue = new LinkedList<>();
        private Entry<String> next;

        public WalkTree() {
            next = root;
        }

        public boolean hasNext() {
            return next != null;
        }

        public Entry<String> getNext() {
            if (!hasNext()) throw new NoSuchElementException();
            Entry<String> current = next;
            if (current.leftChild != null) queue.offer(current.leftChild);
            if (current.rightChild != null) queue.offer(current.rightChild);
            next = queue.isEmpty() ? null : queue.poll();
            return current;
        }
    }

    static class Entry<T> implements Serializable {
        String elementName;
        boolean availableToAddLeftChildren, availableToAddRightChildren;
        Entry<T> parent, leftChild, rightChild;

        public Entry(String elementName) {
            this.elementName = elementName;
            availableToAddLeftChildren = true;
            availableToAddRightChildren = true;
        }

        public boolean isAvailableToAddChildren() {
            return availableToAddLeftChildren || availableToAddRightChildren;
        }
    }
}
