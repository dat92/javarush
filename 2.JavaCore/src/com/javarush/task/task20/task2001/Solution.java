package com.javarush.task.task20.task2001;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
Читаем и пишем в файл: Human
*/
public class Solution {
    public static void main(String[] args) {
        //исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name = File.createTempFile( "test" + File.separatorChar +"ser2001", null);
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            Human ivanov = new Human("Ivanov", new Asset("home", 999_999.99), new Asset("car", 2999.99));
            ivanov.save(outputStream);
            outputStream.flush();

            Human somePerson = new Human();
            somePerson.load(inputStream);
            inputStream.close();
            //check here that ivanov equals to somePerson - проверьте тут, что ivanov и somePerson равны
            System.out.println(ivanov.equals(somePerson));

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class Human {
        public String name;
        public List<Asset> assets = new ArrayList<>();

        public Human() {
        }

        public Human(String name, Asset... assets) {
            this.name = name;
            if (assets != null) {
                this.assets.addAll(Arrays.asList(assets));
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Human human = (Human) o;

            if (name != null ? !name.equals(human.name) : human.name != null) return false;
            return assets != null ? assets.equals(human.assets) : human.assets == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (assets != null ? assets.hashCode() : 0);
            return result;
        }

        public void save(OutputStream outputStream) throws Exception {
            //implement this method - реализуйте этот метод
            if (null == name) {
                outputStream.write(48); //ZERO (ASCII) null person
                outputStream.flush();
                return;
            }

            if (! assets.isEmpty()) {
                outputStream.write(50); //TWO (ASCII) - have asset
            } else {
                outputStream.write(49); //ONE - name only
            }
            outputStream.write((name+"\n").getBytes());

            for (Asset cur : assets) {
                outputStream.write((cur.getName() + "\n").getBytes());
                outputStream.write((Double.toString(cur.getPrice()) + "\n").getBytes());
            }

            outputStream.write(3); //End of text
            outputStream.flush();
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            int isAsset = inputStream.read();

            if (isAsset == 48) return;
            else if (isAsset > 48) {
                name = readString(inputStream);
            }

            if (isAsset == 50) {
                while (inputStream.available() > 0) {
                    String nameAsset = readString(inputStream);
                    if (nameAsset.equalsIgnoreCase("")) break;
                    double doubleAsset = Double.parseDouble(readString(inputStream));
                    Asset curAsset = new Asset(nameAsset, doubleAsset);
                    assets.add(curAsset);
                }
            }
        }

        private String readString (InputStream inputStream) throws Exception {
            StringBuilder sb = new StringBuilder();
            while (inputStream.available() > 0) {
                int cur = inputStream.read();
                if (cur == 10 || cur == 3) break; //LF or EOF
                sb.append((char) cur);
            }
            return sb.toString();
        }
    }
}
