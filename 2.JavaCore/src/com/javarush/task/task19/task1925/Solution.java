package com.javarush.task.task19.task1925;

/* 
Длинные слова
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Err args");
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]));
             BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]))) {
            StringBuilder sb = new StringBuilder();
            while (reader.ready()) {
                String[] currentLine = reader.readLine().split(" ");
                for (String currentWord : currentLine) {
                    if (currentWord.length() > 6) {
                        sb.append(currentWord).append(",");
                    }
                }
            }
            sb.deleteCharAt(sb.length()-1);
            writer.write(sb.toString());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
