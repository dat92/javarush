package com.javarush.task.task22.task2211;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

/* 
Смена кодировки
*/
public class Solution {
    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            System.err.println("Wrong arguments");
            return;
        }
        Charset w1251 = Charset.forName("Windows-1251");
        Charset utf = Charset.forName("UTF-8");


        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(new FileInputStream(args[0]), w1251)
        );
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(args[1]), utf)
        )) {
            while (reader.ready()) {
                writer.write(reader.readLine());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
