package com.javarush.task.task27.task2712.statistic.event;

import com.javarush.task.task27.task2712.RestaurantDate;
import com.javarush.task.task27.task2712.ad.Advertisement;

import java.util.Date;
import java.util.List;

public class VideoSelectedEventDataRow implements EventDataRow {
    private Date currentDate;
    private List<Advertisement> optimalVideoSet;
    private long amount;
    private int totalDuration;

    public VideoSelectedEventDataRow(
            List<Advertisement> optimalVideoSet, //список роликов для показа
                                     long amount, //сумма денег в коп.
                                     int totalDuration //Общая продолжительность показа
    ) {
        currentDate = new Date(RestaurantDate.getInstance().getTime());
        this.optimalVideoSet = optimalVideoSet;
        this.amount = amount;
        this.totalDuration = totalDuration;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public EventType getType() {
        return EventType.SELECTED_VIDEOS;
    }

    @Override
    public Date getDate() {
        return currentDate;
    }

    @Override
    public int getTime() {
        return totalDuration;
    }
}
