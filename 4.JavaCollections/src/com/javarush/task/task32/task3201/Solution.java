package com.javarush.task.task32.task3201;

import java.io.IOException;
import java.io.RandomAccessFile;

/*
Запись в существующий файл
*/
public class Solution {
    public static void main(String... args) {
        try {
            RandomAccessFile accessFile = new RandomAccessFile(args[0], "rw");
            int number = Integer.parseInt(args[1]);
            accessFile.seek(accessFile.length() < number ? accessFile.length() : number);
            accessFile.write(args[2].getBytes());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
