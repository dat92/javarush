package com.javarush.task.task20.task2025;

import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

/*
Алгоритмы-числа
*/
public class Solution {
    /*
    Массив степеней для ускорения процесса подсчёта
    первый уровень - числа от 0 до 9
    второй - степень от 0 до N
    т.е. degreeArr[4][3] = 64 (4^3)
     */
    static long[][] degreeArr;

    //Оригинальное переданное число
    static long sourceN;

    //Текущее число разложенное на цифры
    static int[] currNumb;
    /*
    Временный результат
    Выбор сделан в пользу TreeSet т.к.
    1) Выборка чисел происходит в обратном порядке, а результат должен быть
    в порядке возрастания
    2) Имеет место быть "коллизия", дубли.
    Пример: число 11 имеет сумму степеней равную 2. 2 - является числом Армстронга.
    Далее само число 2 имеет сумму степеней равную 2.
     */
    static TreeSet<Long> results;

    /**
     * Класс для заполнения таблицы со степенями
     */
    static class FillTable implements Runnable {
        Thread t;
        int num, degree;

        FillTable(int num, int degr) {
            this.num = num;
            this.degree = degr;
            t = new Thread(this, num+"");
            t.start();
        }

        @Override
        public void run() {
            for (int i = 0; i<= degree; i++) {
                degreeArr[num][i] = num;
                for (int j = 1; j< i; j++) {
                    degreeArr[num][i] *= num;
                }
            }
        }
    }

    /**
     * Заполняет таблицу со степенями
     */

    private static void setDegree() {
        //Степень числа == кол-во цифр в числе
        //Берём с запасом (разрядность*2),
        // т.к. сумма степеней числа может быть больше
        int degr = (getNumLen(sourceN)) * 2;
        degreeArr = new long[10][degr+1];

        FillTable[] fillTables = new FillTable[10];
        for (int num=0; num<fillTables.length; num++) {
            fillTables[num] = new FillTable(num, degr);
        }

        try {
            for (FillTable cur : fillTables) {
                cur.t.join();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Преобразует setTree в массив примитивных типов
     * @return - массив примитивных типов
     */
    private static long[] setTreeToArrayPrimit() {
        long[] res = new long[results.size()-1];
        Iterator<Long> iterator = results.iterator();
        int i=0;
        while (iterator.hasNext()) {
            long cur = iterator.next();
            if (cur == 0) continue; //Ноль не натуральное число
            res[i] = cur;
            i++;
        }
        Arrays.sort(res);
        return res;
    }

    /**
     * Метод из задания
     * @param N - граница числа
     * @return - массив с числами Армстронга в интервале 1-N
     */
    public static long[] getNumbers(long N) {
        if (N < 1) {
            return new long[0];
        }
        sourceN = N;
        setDegree();
        int sizeLimit = getNumLen(N);
        results = new TreeSet<>();
        //Записываем в массив число N
        currNumb = getArrNum(N, sizeLimit);
        //Начинаем его "декрементировать"
        decrement(0);
        return setTreeToArrayPrimit();
    }

    /**
     * Рекурсивный метод для декремента числа
     * @param discharge - рязряд для декремента
     */
    private static void decrement(int discharge) {
        //Сразу определяем не декрементируем ли последний разряд
        boolean lastDisch = currNumb.length - discharge == 1;
        /*
        Вычисляем границу итерации.
        Если у нас первый разряд, то граница декремента ноль,
        если нет - то границей будет предыдущее число.
        Ведующие нули сохраняем.
        Выбираем числа в которых цифры расположены по возрастанию:
        123, 149, 45559, 0407 и т.д.
        Нет смысла перебирать два и более числа у которых те же цифры:
        на сумму это не влияет.
        Идём следующим шагом:
        999
        899
        889
        888
        799
        ...
        044
        033
        Это позволяет не перебирать все числа от N до 1
         */
        int border = (discharge == 0) ?
                0 : currNumb[discharge-1];
        for (int i = currNumb[discharge]; i>=border; i--) {
            if (!lastDisch) {
                //Выполняем декремент следующего разряда если он есть
                decrement(discharge+1);
            }
            if (lastDisch) {
                /*
                Т.к. итерация выполниться как минимум один раз,
                проверяем текущее число
                 */
                checkNumber(currNumb);
            }

            //Выполняем декремент текущего разряда
            currNumb[discharge]--;
            if (!lastDisch) {
                /*
                После декремента, необходимо сбросить предыдущий
                разряд на 9, чтобы иметь возможность выполнять
                декремент в последующей рекурсии.
                 */
                currNumb[discharge + 1] = 9;
            }
        }
    }

    /**
     * Считает сумму степеней числа
     * @param arr - массив, содержащий разложенное число
     * @param len - длинна массива: массив может содержать нули
     *            в начале и этот параметр показывает нужно ли их
     *            учитывать (положение нулей на результат не влияет)
     * @return - сумма степенией
     */
    private static long getSumOfDegree(int[] arr, int len) {
        /*
        Ноль в любой степени - ноль и на результат не влияет
        Нужно знать в какую степень возводить и ноль не в начале
        влияет на степень
         */
        long summ = 0;
        for (int cur : arr) {
            summ+=degreeArr[cur][len];
        }
        return summ;
    }

    /**
     * Проверка суммы степени числа на число Армстронга
     * @param array - массив с разложенным числом
     */
    private static void checkNumber(int[] array) {
        /*
        Массив может содержать ведущие нули.
        Эти нули влияют не только на степень числа, но и на само число (4007 и 407),
        т.к. мы рассматриваем набор цифр, которые образуют несколько чисел:
        047 это и 470 и 740 и 407 (которое явл числом Армстронга), но сумма степеней
        у всех равна 407.
        Т.е. мы берём число 047 (с тремя разрядами, т.е. со степенью 3!)
        и считаем сумму степеней. Потом эту сумму проверяем, является ли она
        числом Армстронга.
         */
        int curLen = array.length;
        for (int i=0; i<array.length; i++) {
            /*
            В каждой итерации убираем ведующий ноль
            и проверяем уже новое (!) число:
            ((getSumOfDegree(4007) == getSumOfDegree(0047)) !=
            (getSummOfDegree(407) == getSummOfDegree(047)))
             */
            //Получаем сумму степеней текущего числа
            long curSumm = getSumOfDegree(array, curLen);
            //Проверяем является ли эта сумма числом Армстронга
            int len = getNumLen(curSumm);
            int[] arr = getArrNum(curSumm, len);
            long newSumm = getSumOfDegree(arr, len);
            if (curSumm == newSumm && curSumm < sourceN) results.add(curSumm);

            /*
            Сюда попадают следующие условия:
            Переданный массив не содержит ведующих нулеу - первая итерация
            Ведующие нули кончились - не первая итерация
             */
            if (array[i] != 0) return;
            else curLen--;
        }
    }

    /**
     * Получает длинну числа
     * @param n - число
     * @return - длинна int
     */
    private static int getNumLen(long n) {
        int size = 0;
        while (n > 0) {
            n/=10;
            size++;
        }
        return size;
    }

    /**
     * Раскладывает число на цифры
     * @param n - число
     * @param size - вычисленный размер числа
     * @return - массив чисел
     */
    private static int[] getArrNum(long n, int size) {
        int[] numArray = new int[size];
        int val;
        for (int i=size-1; i>-1; i--) {
            val = (int) (n%10);
            numArray[i] = val;
            n/=10;
        }
        return numArray;
    }


    public static void main(String[] args) {

        //long number = 9;
        long number = Long.MAX_VALUE;
        long[] arr = getNumbers(number);
        for (long cur : arr) System.out.println(cur);

    }
}
