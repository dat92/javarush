package com.javarush.task.task25.task2504;

/* 
Switch для нитей
*/
public class Solution {
    public static void processThreads(Thread... threads) {
        //implement this method - реализуйте этот метод
        for (Thread curThread : threads) {
            switch (curThread.getState()) {
                case NEW:
                    curThread.start();
                    break;
                case WAITING:
                case BLOCKED:
                case TIMED_WAITING:
                    curThread.interrupt();
                    break;
                case RUNNABLE:
                    curThread.isInterrupted();
                    break;
                case TERMINATED:
                    System.out.println(curThread.getPriority());
            }
        }
    }

    public static void main(String[] args) {
    }
}
