package com.javarush.task.task27.task2712.ad;

import java.util.ArrayList;
import java.util.List;

public class AdvertisementStorage {

    private static AdvertisementStorage storage = new AdvertisementStorage();
    private final List<Advertisement> videos = new ArrayList<>();

    private AdvertisementStorage() {
        Object someContest = new Object();
        videos.add(new Advertisement(someContest, "First Video",
                5000, 100, 3 * 60)); // 3 min
        videos.add(new Advertisement(someContest, "Second Video",
                100, 10, 15 * 60)); //15 min
        videos.add(new Advertisement(someContest, "Third Video",
                400, 1, 10 * 60)); // 10 min
        videos.add(new Advertisement(someContest, "Самая надоедливая реклама",
                10000, 1000, 30 * 60));
        videos.add(new Advertisement(someContest, "Купил эту хрень, БЫСТРААА!!!",
                100000, 0,  20));
        videos.add(new Advertisement(someContest, "Купите это... Ну, пожалуйста...",
                50, 5, 2 * 60));
        videos.add(new Advertisement(someContest, "Copy First Video",
                5000, 100, 3 * 60));
    }

    public static AdvertisementStorage getInstance() {
        return storage;
    }

    public List<Advertisement> list() {
        return videos;
    }

    public void add(Advertisement advertisement) {
        videos.add(advertisement);
    }
}
