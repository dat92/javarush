package com.javarush.task.task19.task1922;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/* 
Ищем нужные строки
*/

public class Solution {
    public static List<String> words = new ArrayList<String>();

    static {
        words.add("файл");
        words.add("вид");
        words.add("В");
    }

    public static void main(String[] args) {
        String fileName;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            fileName = reader.readLine();
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            while (reader.ready()) {
                String currentString = reader.readLine();
                String[] currentArray = currentString.split(" ");
                int count = 0;
                for (String curList : words) {
                    for (String curWord : currentArray) {
                        if (curList.equalsIgnoreCase(curWord)) count++;
                        if (count > 2) break;
                    }
                }

                if (count == 2) System.out.println(currentString);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return;
        }
    }
}
