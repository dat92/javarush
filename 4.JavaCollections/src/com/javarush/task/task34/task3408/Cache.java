package com.javarush.task.task34.task3408;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.WeakHashMap;

public class Cache<K, V> {
    private Map<K, V> cache = new WeakHashMap<>();

    public V getByKey(K key, Class<V> clazz) throws Exception {
        V result = cache.get(key);
        if (result == null) {
            result = clazz.getConstructor(key.getClass()).newInstance(key);
            cache.put(key, result);
        }
        return result;
    }

    public boolean put(V obj) {
        try {
            Method getKeyMethod = obj.getClass().getDeclaredMethod("getKey");
            getKeyMethod.setAccessible(true);
            K key = (K) getKeyMethod.invoke(obj);
            cache.put(key, obj);
            return true;
        }
        catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            return false;
        }
    }

    public int size() {
        return cache.size();
    }
}
