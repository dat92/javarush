package com.javarush.task.task28.task2805;

import java.util.concurrent.atomic.AtomicInteger;

public class MyThread extends Thread {
    private static AtomicInteger priorityCount = new AtomicInteger(0);

    {
        int priority = (priorityCount.getAndIncrement() % 10) + 1;
        //JVM сама не даст сделать приоритет выше, чем у группы
        setPriority(priority);
    }


    public MyThread() {
    }

    public MyThread(Runnable runnable) {
        super(runnable);
    }

    public MyThread(ThreadGroup threadGroup, Runnable runnable) {
        super(threadGroup, runnable);
    }

    public MyThread(String s) {
        super(s);
    }

    public MyThread(ThreadGroup threadGroup, String s) {
        super(threadGroup, s);
    }

    public MyThread(Runnable runnable, String s) {
        super(runnable, s);
    }

    public MyThread(ThreadGroup threadGroup, Runnable runnable, String s) {
        super(threadGroup, runnable, s);
    }

    public MyThread(ThreadGroup threadGroup, Runnable runnable, String s, long l) {
        super(threadGroup, runnable, s, l);
    }


}
