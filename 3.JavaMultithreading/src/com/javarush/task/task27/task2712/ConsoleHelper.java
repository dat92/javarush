package com.javarush.task.task27.task2712;

import com.javarush.task.task27.task2712.kitchen.Dish;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ConsoleHelper {
    private static BufferedReader reader
            = new BufferedReader(new InputStreamReader(System.in));

    public static void writeMessage(String message) {
        System.out.println(message);
    }

    public static String readString() throws IOException {
        return reader.readLine();
    }

    public static List<Dish> getAllDishesForOrder() throws IOException {
        writeMessage(Dish.allDishesToString());
        List<Dish> dishes = new ArrayList<>();
        String selectDishStr;

        while (true) {
            writeMessage("Enter dish:");
            selectDishStr = readString();
            if (selectDishStr.equalsIgnoreCase("exit")) return dishes;
            try {
                Dish selectDish = Dish.valueOf(selectDishStr);
                dishes.add(selectDish);
            } catch (IllegalArgumentException ex) {
                System.out.printf("Dish %s not found%n", selectDishStr);
            }
        }
    }
}
