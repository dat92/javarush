package com.javarush.task.task35.task3513;

import java.util.*;

public class Model {
    private final static int FIELD_WIDTH = 4;
    private Tile[][] gameTiles;
    int score = 0, maxTile = 0;
    private Stack<Tile[][]> previousStates = new Stack<>();
    private Stack<Integer> previousScores = new Stack<>();
    private boolean isSaveNeeded = true;

    Tile[][] getGameTiles() {
        return gameTiles;
    }

    public Model() {
        resetGameTiles();
    }

    void resetGameTiles() {
        gameTiles = new Tile[Model.FIELD_WIDTH][Model.FIELD_WIDTH];
        for (int i = 0; i < Model.FIELD_WIDTH; i++) {
            for (int j = 0; j < Model.FIELD_WIDTH; j++) {
                gameTiles[i][j] = new Tile();
            }
        }
        addTile();
        addTile();
    }

    private void addTile() {
        List<Tile> emptyTile = getEmptyTiles();
        if (emptyTile.size() == 0) return;
        Tile changedTile = emptyTile.get((int) (emptyTile.size() * (Math.random())));
        changedTile.value = Math.random() < 0.9 ? 2 : 4;
    }

    private List<Tile> getEmptyTiles() {
        List<Tile> emptyTile = new LinkedList<>();
        for (Tile[] curArr : gameTiles) {
            for (Tile curTile : curArr) {
                if (curTile.isEmpty()) emptyTile.add(curTile);
            }
        }
        return emptyTile;
    }

    void left() {
        if (isSaveNeeded) saveState(gameTiles);
        boolean isChange = false;
        for (Tile[] cur : gameTiles) {
            isChange = compressTiles(cur) | mergeTiles(cur);
        }

        if (isChange) addTile();
        isSaveNeeded = true;
    }

    void right() {
        saveState(gameTiles);
        turnLeftAndBack(2);
    }

    void down() {
        saveState(gameTiles);
        turnLeftAndBack(1);
    }

    void up() {
        saveState(gameTiles);
        turnLeftAndBack(3);
    }

    private void turnLeftAndBack(int count) {
        Tile[][] newTiles = gameTiles;
        for (int i = 0; i < count; i++) {
            newTiles = turnClockwise(newTiles);
        }
        gameTiles = newTiles;
        left();
        newTiles = gameTiles;
        for (int i = 0; i < count; i++) {
            newTiles = turnCounterClockwise(newTiles);
        }
        gameTiles = newTiles;
    }

    private Tile[][] turnClockwise(Tile[][] srcArray) {
        Tile[][] result = new Tile[srcArray.length][];
        for(int i = 0; i < srcArray.length; i++) {
            Tile[] tmpArray = new Tile[srcArray[i].length];
            for (int j = srcArray[i].length-1, k = 0; j >= 0; j--, k++) {
                tmpArray[k] = srcArray[j][i];
            }
            result[i] = tmpArray;
        }
        return result;
    }

    private Tile[][] turnCounterClockwise(Tile[][] srcArray) {
        Tile[][] result = new Tile[srcArray.length][];
        for (int i = 0; i < srcArray.length; i++) {
            result[i] = new Tile[srcArray[i].length];
        }
        for(int i = srcArray[0].length - 1; i >= 0; i--) {
            for (int j = srcArray.length - 1, k = 0; j >= 0; j--, k++) {
                result[k][i] = srcArray[i][j];
            }
        }
        return result;
    }

    private boolean compressTiles(Tile[] tiles) {
        boolean isChange = false;
        for (int i = 0; i < tiles.length - 1; i++) {
            if (tiles[i].isEmpty()) {
                boolean isFoundNoEmpty = false;
                //If we have row of empty tiles between
                for (int j = i; j < tiles.length-1; j++) {
                    if (!tiles[j + 1].isEmpty()) {
                        Tile empty = tiles[i];
                        tiles[i] = tiles[j+1];
                        tiles[j+1] = empty;
                        isFoundNoEmpty = true;
                        isChange = true;
                        break;
                    }
                }
                if (!isFoundNoEmpty) break;
            }
        }
        return isChange;
    }

    private boolean mergeTiles(Tile[] tiles) {
        boolean isChange = false;
        for (int i = 0; i < tiles.length-1; i++) {
            if (!tiles[i].isEmpty() && tiles[i].value == tiles[i+1].value) {
                int incrementScore = tiles[i].value * 2;
                tiles[i].value = incrementScore;
                score += incrementScore;
                tiles[i+1].value = 0;
                if (maxTile < incrementScore) maxTile = incrementScore;
                isChange = true;
                i++;
            }
        }
        if (compressTiles(tiles)) isChange = true;
        return isChange;
    }

    boolean canMove() {
        for (int i = 0; i < gameTiles.length; i++) {
            for (int j = 0; j < gameTiles[i].length; j++) {
                if (gameTiles[i][j].isEmpty()) return true;
                if (j < gameTiles[i].length - 1) {
                    if (gameTiles[i][j].value == gameTiles[i][j+1].value) return true;
                }
                if (i < gameTiles.length - 1) {
                    if (gameTiles[i][j].value == gameTiles[i+1][j].value) return true;
                }
            }
        }
        return false;
    }

    private void saveState(Tile[][] tiles) {
        previousScores.push(score);
        //Tile[][] newTiles = Arrays.copyOf(tiles, tiles.length);
        Tile[][] newTiles = new Tile[tiles.length][];
        for (int i = 0; i < tiles.length; i++) {
            newTiles[i] = new Tile[tiles[i].length];
            for (int j = 0; j < tiles[i].length; j++) {
                newTiles[i][j] = new Tile(tiles[i][j].value);
            }
        }
        previousStates.push(newTiles);
        isSaveNeeded = false;
    }

    void rollback() {
        if (!previousScores.empty() && !previousStates.empty()) {
            score = previousScores.pop();
            gameTiles = previousStates.pop();
        }
    }

    void randomMove() {
        int move = ((int) (Math.random() * 100)) % 4;
        switch (move) {
            case 0:
                left();
                break;
            case 1:
                right();
                break;
            case 2:
                up();
                break;
            case 3:
                down();
        }
    }

    private boolean hasBoardChanged() {
        Tile[][] prevTiles = previousStates.peek();
        for (int i = 0; i < gameTiles.length; i++) {
            for (int j = 0; j < gameTiles[i].length; j++) {
                if (gameTiles[i][j].value != prevTiles[i][j].value) return true;
            }
        }
        return false;
    }

    private MoveEfficiency getMoveEfficiency(Move move) {
        move.move();
        MoveEfficiency result;
        if (hasBoardChanged()) {
            int numberOfEmpty = 0;
            for (Tile[] curArr : gameTiles) {
                for (Tile curTile : curArr) {
                    if (curTile.isEmpty()) numberOfEmpty++;
                }
            }
            result = new MoveEfficiency(numberOfEmpty, score, move);
        } else {
            result = new MoveEfficiency(-1, 0, move);
        }

        rollback();
        return result;
    }

    void autoMove() {
        PriorityQueue<MoveEfficiency> queue = new PriorityQueue<>(4, Collections.reverseOrder());
        queue.offer(getMoveEfficiency(this :: left));
        queue.offer(getMoveEfficiency(this :: right));
        queue.offer(getMoveEfficiency(this :: up));
        queue.offer(getMoveEfficiency(this :: down));

        queue.peek().getMove().move();
    }
}
