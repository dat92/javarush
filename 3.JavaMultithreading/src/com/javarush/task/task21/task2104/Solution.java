package com.javarush.task.task21.task2104;

import java.util.HashSet;
import java.util.Set;

/* 
Equals and HashCode
*/
public class Solution {
    private final String first, last;

    public Solution(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public boolean equals(Object n) {
        if (null == n) return false;
        if (n == this) return true;
        if (!(n instanceof Solution)) return false;
        Solution sol = (Solution) n;
        return (((null == first) && (null == sol.first)) || (sol.first.equals(first)))
                && (((null == last) && (null == sol.last)) || (sol.last.equals(last)));
    }

    public int hashCode() {
        int result=0;
        result += first == null ? 0 : first.hashCode();
        result += last == null ? 0 : last.hashCode();
        return 37 * result;
    }

    public static void main(String[] args) {
        Set<Solution> s = new HashSet<>();
        s.add(new Solution("Donald", "Duck"));
        System.out.println(s.contains(new Solution("Donald", "Duck")));
    }
}
